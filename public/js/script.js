  $(function () {

      //Datemask dd/mm/yyyy
      $('#dmask').inputmask('yyyy-mm-dd', {
          'placeholder': 'yyyy-mm-dd',
          rightAlign: true
      });
      $('#mmask,#mmask2').inputmask('01999999999', {
          'placeholder': '01_________',
          rightAlign: true
      });
      $('#graduation_year').inputmask('9999', {
          'placeholder': '____',
          rightAlign: true
      });
      $('#Sid').inputmask('99999999999999');
      $('#area').select2({
          placeholder: 'اﺩﺧﻞ اﻟﻤﻨﻄﻘﺔ اﻟﺴﻜﻨﻴﺔ',
          maximumSelectionLength: 2
      });
      //Gender Selection
      $('input[name="gender"]').click(function () {
          if ($('input[name="gender"]').val() && $(this).val() == 1) {
              $('#opMale,#military').css('display', 'block');
              $('#opFemale').css('display', 'none');
          } else {
              $('#opMale,#military').css('display', 'none');
              $('#opFemale').css('display', 'block');
          }
          $(this).parents('.btn-group').find('label.btn').removeClass('active');
          if ($(this).prop('checked')) {
              $(this).parents('label.btn').addClass('active');
          }
      });

      //Gender Selection
      $('input[name="edu"]').click(function () {
          var college = $('.college-year');
          var graduation = $('.graduation_year');
          if ($('input[name="edu"]').val() && $(this).val() == 1) {
              $('#college-year').removeAttr('required');
              college.css('display', 'none');
              graduation.css('display', 'block');
              if ($('#graduation_year').prop("required") == false) {
                  $('#graduation_year').prop('required', true);
              }
          } else {
              $('#graduation_year').removeAttr('required');
              graduation.css('display', 'none');
              college.css('display', 'block');
              if ($('#college-year').prop("required") == false) {
                  $('#college-year').prop('required', true);
              }
          }
          $(this).parents('.btn-group').find('label.btn').removeClass('active');
          if ($(this).prop('checked')) {
              $(this).parents('label.btn').addClass('active');
          }
      });

      //Car Drive
      $('input[name="cdrive"]').click(function () {
          console.log(this);
          $(this).parents('.btn-group').find('label.btn').removeClass('active');
          if ($(this).prop('checked')) {
              $(this).parents('label.btn').addClass('active');
          }
      });

      //Car Drive
      $('select[name="hRelatives"]').change(function () {
          if ($(this).val() == "1") {
              $('.hRelativesN').css('display', 'block');
          } else {
              $('.hRelativesN').css('display', 'none');
          }
      });
  });