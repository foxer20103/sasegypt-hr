<?php
namespace App\DataTables;

use App\User;
use Yajra\DataTables\Services\DataTable;

class UserDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', function ($user) {
                return '<a href="' . route('user.edit', $user->id) . '" class="btn btn-success btn-sm">تعديل</a>
          <button data-remote="' . route('user.destroy', $user->id) . '" class="btn-delete btn btn-success btn-sm">حذف</button>';
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        return $this->data;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['title' => 'العمليات', 'width' => '17%'])
            ->parameters([
                'dom'     => 'B',
                'buttons' => [
                    ['text' => '<i class="fa fa-plus"></i>اضافة مستخدم', 'className' => 'btn btn-info', 'action' => 'function(){
                        window.location.href="' . route('user.create') . '";
                    }',
                    ],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id'    => ['title' => "#"],
            'name'  => ['title' => "الاسم"],
            'email' => ['title' => "البريد الالكتروني"],
        ];
    }
}
