<?php

namespace App\DataTables;

use App\Applicants\applicant;
use Yajra\DataTables\Services\DataTable;

class applicantDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', function ($applicant) {
                $buttons = "";
                if (user()->hasRole('write')) {
                    $buttons .=
                    '<a href="' . route('app.edit', $applicant->id) . '" class="btn btn-success btn-sm">تعديل</a>';
                }
                $buttons .= '<a target="_blank" href="' . route('app.print', $applicant->id) . '" class="btn btn-success btn-sm">طباعة</a>';
                if (user()->hasRole('backup')) {
                    $buttons .=
                    '<a target="_blank" href="' . route('app.download', $applicant->id) . '" class="btn btn-success btn-sm">تحميل</a>';
                }
                if (user()->hasRole('delete')) {
                    $buttons .=
                    '<button data-remote="' . route('app.destroy', $applicant->id) . '" class="btn-delete btn btn-success btn-sm">حذف</button>';
                }
                return $buttons;
            })
            ->editColumn('b_date', function ($applicant) {
                return $applicant->b_date ? round((time() - strtotime($applicant->b_date)) / 31536000) : '';
            });
    }

/**
 * Get query source of dataTable.
 *
 * @param \App\User $model
 * @return \Illuminate\Database\Eloquent\Builder
 */
    public function query(applicant $model)
    {
        return $this->data->select('id', 'name', 'area', 'b_date')->orderBy('updated_at', 'desc');
    }

/**
 * Optional method if you want to use html builder.
 *
 * @return \Yajra\DataTables\Html\Builder
 */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['title' => 'العمليات', 'width' => '21%'])
            ->parameters([]);
    }

/**
 * Get columns.
 *
 * @return array
 */
    public function getColumns()
    {
        return [
            'id'     => ['title' => '#'],
            'name'   => ['title' => 'الأسم'],
            [
                'name'       => 'in_area.name',
                'data'       => 'in_area.name',
                'title'      => 'المنطقة',
                'searchable' => false,
                'orderable'  => false,
            ],
            'b_date' => ['title' => 'العمر'],
        ];
    }
}
