<?php
namespace App\DataTables;

use App\Applicants\Area;
use Yajra\DataTables\Services\DataTable;

class AreaDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', function ($Area) {
                return '<a href="' . route('Area.edit', $Area->area_id) . '" class="btn btn-success btn-sm">تعديل</a>';
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Area $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Area $model)
    {
        return $this->data;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['title' => 'العمليات', 'width' => '17%'])
            ->parameters([
//                'dom'     => 'Blrtip',
                'buttons' => [
                    ['text' => '<i class="fa fa-plus"></i>اضافة منطقة', 'className' => 'btn btn-info', 'action' => 'function(){
                        window.location.href="' . route('Area.create') . '";
                    }',
                    ],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'area_id' => ['title' => "رقم المنطقة"],
            'name'    => ['title' => "الاسم"],
        ];
    }
}
