<?php

namespace App\Http\Controllers;

use App\Applicants\applicant;
use App\Applicants\Area;
use App\Applicants\branch;
use App\DataTables\applicantDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class SearchController extends Controller
{
    /**
     * @param Request $request
     * @param applicant $applicant
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function filter(Request $request, applicant $applicant, applicantDataTable $app)
    {
        $applicant = $applicant->newQuery();
        // Search for a user based on their Area.
        if ($request->has('branch') and request('branch') !== '') {
            $applicant->whereHas('inArea', function ($query) {
                $branch = branch::select('id')->where('id', '=', request('branch'))->with(['selection' => function ($query) {
                    $query->select('areas.area_id');
                }])->first();
                $query->whereIn('area_id', $branch->selection);
            });
        }

        if ($request->has('area') and request('area') !== '') {
            $applicant->whereHas('inArea', function ($query) {
                $query->where('area_id', request('area'));
            });
        }

        // Search for a user based on their Rate.
        if ($request->has('Frate') and request('Frate') !== '') {
            $applicant->where('Frate', $request->input('Frate'));
        }

        // Search for a user based on their Education.
        if ($request->has('edu') and request('edu') !== '') {
            $applicant->where('edu', $request->input('edu'));
        }

        // Search for a user based on their Education.
        if ($request->has('gender') and request('gender') !== '') {
            $applicant->where('gender', $request->input('gender'));
        }

        // Search for a user based on their Age.
        if ($request->has('b_date') and request('b_date') !== '') {
            if ($request->input('b_date') == 1) {
                $start = 0;
                $end   = 18;
            } elseif ($request->input('b_date') == 2) {
                $start = 18;
                $end   = 25;
            } elseif ($request->input('b_date') == 3) {
                $start = 25;
                $end   = 200;
            } else {
                $start = 0;
                $end   = 0;
            }

            $start = Carbon::today()->subYears($start)->toDateTimeString();
            $end   = Carbon::today()->subYears($end)->toDateTimeString();
            $applicant->whereBetween('b_date', [$end, $start]);
        }
        // Search for a user based on their Age.
        if ($request->has('created_at') and request('created_at') !== '') {
            $created_at = request('created_at');

            $start = Carbon::create($created_at, 1, 1, 0, 0, 0)->toDateTimeString();
            $end   = Carbon::create($created_at, 12, 31, 0, 0, 0)->toDateTimeString();
            $applicant->whereBetween('created_at', [$start, $end]);
        }
        // Search for a user based on their Age.
        if ($request->has('interSat') and request('interSat') !== '') {
            $applicant->where('interSat', $request->input('interSat'));
        }
        // Search for a user based on their Expetion.
        if ($request->has('apply') and request('apply') !== '' and request('expTime') == '') {
            $applicant->where(['apply' => request('apply')]);
        }

        $apps    = $applicant->with('inArea')->select('id', 'name', 'area', 'b_date');
        $areas   = Area::all();
        $branchs = branch::all();

        return $app->with(['data' => $apps])->render('applicant.index', compact('areas', 'branchs'));
    }
}
