<?php
namespace App\Http\Controllers\backup;

use App\Applicants\applicant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Mpdf\Mpdf;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

ini_set('max_execution_time', 0);
ini_set('memory_limit', '165M');

class backup
{
    protected $headers = [
        'Cache-Control' => 'cache, must-revalidate',
        'Content-Type'  => 'charset=utf-8',
    ];

    public function backupProcess(Request $request, MessageBag $message)
    {
        $date  = Carbon::parse($request->EDate)->addDay();
        $check = $date->diffInDays($request->FDate);
        if ($check >= 33) {
            $message->add('error', 'الرجاء تحديد مدة اقل حيث ان اقصى مدة هي شهر');
            return redirect(route('app.backup'))->withErrors($message);
        }
        $applicants = applicant::with('inArea', 'experiences')->whereBetween('created_at', [$request->FDate, $date])->get();
        if (request('type') == 'xlsx') {
            return $this->xlsx($applicants);
        } elseif (request('type') == 'pdf') {
            return $this->BackupPdf($request);
        } else {
            return request('type');
        }
    }

    public function download($id)
    {
        $applicants = applicant::with('inArea', 'experiences')->where('id', $id)->get();
        $file       = $this->pdf($applicants);
        return response()->download($file, $id . '.pdf', $this->headers);
    }

    public function BackupPdf(request $request)
    {
        $date       = Carbon::parse($request->EDate)->addDay();
        $applicants = applicant::with('inArea', 'experiences')->whereBetween('created_at', [$request->FDate, $date])->get();
        $file       = $this->pdf($applicants);
        return response()->download($file, date('Y-m-d') . '.pdf', $this->headers);
    }

    public function pdf($applicants)
    {
        ob_end_clean();
        $logger = new Logger('name');
        $logger->pushHandler(new StreamHandler('zeroGo', Logger::DEBUG));

        $mpdf = new Mpdf([
            'orientation'   => 'P',
            'format'        => 'A4',
            'margin_left'   => '5',
            'margin_right'  => '5',
            'margin_top'    => '5',
            'margin_bottom' => '5',
            'margin_header' => '0',
            'margin_footer' => '0']);

        $mpdf->setLogger($logger);
        $mpdf->SetDirectionality('rtl');
        $mpdf->showImageErrors  = true;
        $mpdf->autoScriptToLang = true;
        $mpdf->autoLangToFont   = true;

        $header = view('applicant.backup.header')->render();
        $mpdf->writeHtml($header);
        $i     = 0;
        $count = count($applicants);
        foreach ($applicants as $key => $app) {
            $i++;
            $data = $this->GetData($app);
            $view = view('applicant.backup.printer')->with($data)->render();
            $mpdf->writeHtml($view);
            if ($i !== $count) {
                $mpdf->addPage();
            }
        }
        $mpdf->writeHtml('</body></html>');
        $file = storage_path('backup.pdf');
        $mpdf->output($file, 'F');
        return $file;
    }

    public function GetData($app)
    {
        //Handel Data
        $name = $app->name;
        /* Profile Image */
        if ($app->img !== null) {
            $ProfileImage = $app->img;
        } else {
            $ProfileImage = null;
        }
        $BirthOfDate = $app->b_date;
        $LivingArea  = $app->inArea->name;
        /* Mobile */
        if (!empty($app->mobile2)) {
            $MobileNumber = $app->mobile . ' / ' . $app->mobile2;
        } else {
            $MobileNumber = $app->mobile;
        }
        /* Gender */
        if ($app->gender == '1') {
            $Gender = "ﺫﻛﺮ";
        } else {
            $Gender = "اﻧﺜﻲ";
        }
        /* Marital By Gender */
        if ($app->gender == '1') {
            if ($app->marital == '0') {
                $MaritalStatue = "ﺃﻋﺰﺏ";
            } elseif ($app->marital == '1') {
                $MaritalStatue = "ﻣﺘﺰﻭﺝ";
            } elseif ($app->marital == '2') {
                $MaritalStatue = "ﻣﻄﻠﻖ";
            } elseif ($app->marital == '3') {
                $MaritalStatue = "ﺧﺎﻃﺐ";
            }
        } elseif ($app->gender == '0') {
            if ($app->marital == '0') {
                $MaritalStatue = "ﺃﻧﺴﺔ";
            } elseif ($app->marital == '1') {
                $MaritalStatue = "ﻣﺘﺰﻭﺟﺔ";
            } elseif ($app->marital == '2') {
                $MaritalStatue = "ﻣﻄﻠﻘﺔ";
            } elseif ($app->marital == '3') {
                $MaritalStatue = "ﻣﺨﻄﻮﺑﺔ";
            }
        }
        /* Military Statue */
        if ($app->military == 0) {
            $MilitaryStatue = "ﺃﺩﻯ اﻟﺨﺪﻣﺔ";
        } elseif ($app->military == 1) {
            $MilitaryStatue = "ﺇﻋﻔﺎء ﻣﺆﻗﺖ";
        } elseif ($app->military == 2) {
            $MilitaryStatue = "ﺇﻋﻔﺎء ﻧﻬﺎﺋﻰ";
        } elseif ($app->military == 3) {
            $MilitaryStatue = "ﻟﻢ ﻳﺼﺒﻪ اﻟﺪﻭﺭ";
        }

        if ($app->cdrive == 1) {
            $CanDrive = "ﻧﻌﻢ";
        } else {
            $CanDrive = "ﻻ";
        }

        $UniversityName = $app->university;
        $CollegeName    = $app->college;
        $Edu            = $app->edu;
        if ($Edu == 0) {
            $StudyYear = $app->college_year;
            $StudyName = "اﻟﻔﺮﻗﺔ";
        } elseif ($Edu == 1) {
            $StudyName = "ﺳﻨﺔ اﻟﺘﺨﺮﺝ ";
            $StudyYear = $app->graduation_year;
        }
        if ($app->cskills == 0) {
            $ComputerSkills = "ﻣﺒﺘﺪﻯء";
        } elseif ($app->cskills == 1) {
            $ComputerSkills = "ﻣﻘﺒﻮﻝ";
        } elseif ($app->cskills == 2) {
            $ComputerSkills = "ﺟﻴﺪ";
        } elseif ($app->cskills == 3) {
            $ComputerSkills = "ﺟﻴﺪ ﺟﺪا";
        } elseif ($app->cskills == 4) {
            $ComputerSkills = "ﻣﻤﺘﺎﺯ";
        }

        if ($app->onjop == 0) {
            $Onjop = "ﻧﻌﻢ";
        } else {
            $Onjop = "ﻻ";
        }

        if ($app->startwork == 0) {
            $WhenStartWork = "ﺃﺳﺒﻮﻉ";
        } elseif ($app->startwork == 1) {
            $WhenStartWork = "ﺃﺳﺒﻮﻋﻴﻦ";
        } elseif ($app->startwork == 2) {
            $WhenStartWork = "ﺷﻬﺮ";
        } elseif ($app->startwork == 3) {
            $WhenStartWork = "ًﻓﻮﺭا";
        }

        if ($app->haveInsurance == 1) {
            $HaveInsurance = "ﻧﻌﻢ";
        } else {
            $HaveInsurance = "ﻻ";
        }
        if ($app->havePension == 1) {
            $HavePension = "ﻧﻌﻢ";
        } else {
            $HavePension = "ﻻ";
        }
        if ($app->hRelatives == 2) {
            $HaveRelatives = "نعم";
        } else {
            $HaveRelatives = "ﻻ";
        }

        $RelativesName = $app->hRelativesN;
        if ($app->howFind == 1) {
            $HowFindUs = "ﻣﻌﺎﺭﻑ";
        } elseif ($app->howFind == 2) {
            $HowFindUs = "اﻟﺘﺠﻮاﻝ";
        } elseif ($app->howFind == 3) {
            $HowFindUs = "اعلان / انترنت";
        } elseif ($app->howFind == 4) {
            $HowFindUs = "اعلان / جريدة";
        } elseif ($app->howFind == 5) {
            $HowFindUs = "ﻣﻮﻗﻊ اﻟﺸﺮﻛﺔ";
        }

        $ExpectedJop = $app->ExJop;
        $Fristrate   = $app->Frate;

        if ($app->interSat == "0") {
            $interSat = "ﻣﻮاﻓﻖ ﻋﻠﻴﻪ";
        } elseif ($app->interSat == "1") {
            $interSat = "ﻣﺮﻓﻮﺽ";
        } elseif ($app->interSat == "2") {
            $interSat = "اﻧﺘﻈﺎﺭ";
        } elseif ($app->interSat == null) {
            $interSat = null;
        }

        if ($app->interBy == "1") {
            $interBy = "ﻣﺤﻤﺪﺯﻫﺮﺓ";
        } elseif ($app->interBy == "2") {
            $interBy = "ﺣﺴﺎﻡﻋﺰﺕ";
        } elseif ($app->interBy == "3") {
            $interBy = "ﻫﺎﻧﻰﻋﺼﺎﻡ";
        } elseif ($app->interBy == "4") {
            $interBy = "ﻋﻤﺮﻭاﻟﺠﺰاﺭ";
        } elseif ($app->interBy == "5") {
            $interBy = "ﻫﺎﻧﻰﻣﺤﻤﺪ";
        } elseif ($app->interBy == "6") {
            $interBy = "ﻣﺆﻣﻦﻋﺒﺪاﻟﺮﺣﻤﻦ";
        } elseif ($app->interBy == null) {
            $interBy = null;
        }

        $interDate = $app->interDate;
        $JobBrand  = $app->JobBrand;

        if ($app->JobBranch == "1") {
            $JobBranch = "ﻋﺒﺎﺱ اﻟﻌﻘﺎﺩ";
        } elseif ($app->JobBranch == "2") {
            $JobBranch = "اﻟﺘﺠﻤﻊ";
        } elseif ($app->JobBranch == "3") {
            $JobBranch = "اﻟﻤﻬﻨﺪﺳﻴﻦ";
        } elseif ($app->JobBranch == "4") {
            $JobBranch = "ﻣﻮﻝ اﻟﻌﺮﺏ";
        } elseif ($app->JobBranch == "5") {
            $JobBranch = "ﻣﻮﻝ ﻣﺼﺮ";
        } elseif ($app->JobBranch == null) {
            $JobBranch = null;
        }

        $JobTitle = $app->JobTitle;

        $CreatedAt = $app->created_at;
        return get_defined_vars();
    }

    public function xlsx($applicants)
    {
        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0);
        $spreadsheet->getDefaultStyle()->applyFromArray(
            [
                'font'      => ['name' => 'Times New Roman', 'size' => 18],
                'alignment' => ['horizontal' => 'right', 'vertical' => 'center', 'readOrder' => 2],
            ]
        );
        $activeSheet = $spreadsheet->getActiveSheet();
        $activeSheet->getPageSetup()->setOrientation('landscape');
        $activeSheet->getPageSetup()->setPaperSize('9');
        $activeSheet->getDefaultRowDimension()->setRowHeight(20);
        $activeSheet->setRightToLeft(true);
        $activeSheet->setTitle('Home');
        $activeSheet->getSheetView()->setZoomScale(70);

        $this->backup($applicants, $activeSheet);
        $file = storage_path('backup.xlsx');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save($file);

        return response()->download($file, date('Y-m-d') . '.xlsx', $this->headers);
    }

    public function fixAr($str)
    {
        return $str;
    }

    public function backup($applicants, $activeSheet)
    {
        ob_end_clean();
        $i = 1;

        $header = ['font' => [
            'bold'      => true,
            'underline' => 'single',
        ],
            'alignment'       => [
                'horizontal' => 'center',
                'vertical'   => 'center',
            ],
        ];
        $activeSheet->getColumnDimension('A')->setWidth('25');
        $activeSheet->getColumnDimension('B')->setWidth('4.5');
        $activeSheet->getColumnDimension('C')->setWidth('6.9');
        $activeSheet->getColumnDimension('D')->setWidth('5');
        $activeSheet->getColumnDimension('E')->setWidth('12.7');
        $activeSheet->getColumnDimension('F')->setWidth('6');
        $activeSheet->getColumnDimension('G')->setWidth('13.3');
        $activeSheet->getColumnDimension('H')->setWidth('17');

        foreach ($applicants as $app) {
            $mobile = !empty($app->mobile2) ? $app->mobile . ' / ' . $app->mobile2
            : $app->mobile;
            $gender = $app->gender == 1 ? 'ﺫﻛﺮ' : 'اﻧﺜﻰ';

            if ($app->marital == 0) {
                $marital = $app->gender == 1 ? 'ﺃﻋﺰﺏ' : 'ﺃﻧﺴﺔ';
            } elseif ($app->marital == 1) {
                $marital = $app->gender == 1 ? 'ﻣﺘﺰﻭﺝ' : 'ﻣﺘﺰﻭﺟﺔ';
            } elseif ($app->marital == 2) {
                $marital = $app->gender == 1 ? 'ﻣﻄﻠﻖ' : 'ﻣﻄﻠﻘﺔ';
            } else {
                $marital = $app->gender == 1 ? 'ﺧﺎﻃﺐ' : 'ﻣﺨﻄﻮﺑﺔ';
            }

            if ($app->military == 0) {
                $military = 'ﺃﺩﻯ اﻟﺨﺪﻣﺔ';
            } elseif ($app->military == 1) {
                $military = 'ﺇﻋﻔﺎء ﻣﺆﻗﺖ';
            } elseif ($app->military == 2) {
                $military = 'ﺇﻋﻔﺎء ﻧﻬﺎﺋﻰ';
            } else {
                $military = 'ﻟﻢ ﻳﺼﺒﻪ اﻟﺪﻭﺭ';
            }

            if ($app->cdrive == 1) {
                $cdrive = 'ﻧﻌﻢ';
            } else {
                $cdrive = 'ﻻ';
            }

            if ($app->haveInsurance == 1) {
                $haveInsurance = 'ﻧﻌﻢ';
            } else {
                $haveInsurance = 'ﻻ';
            }

            if ($app->havePension == 1) {
                $havePension = 'ﻧﻌﻢ';
            } else {
                $havePension = 'ﻻ';
            }

            if ($app->cskills == 0) {
                $cskills = 'ﻣﺒﺘﺪﻯء';
            } elseif ($app->cskills == 1) {
                $cskills = 'ﻣﻘﺒﻮﻝ';
            } elseif ($app->cskills == 2) {
                $cskills = 'ﺟﻴﺪ';
            } else {
                $cskills = 'ﻣﻤﺘﺎﺯ';
            }

            if ($app->edu == 0) {
                $eduTitle = 'اﻟﻔﺮﻗﺔ';
                $eduValue = $app->college_year;
            } else {
                $eduTitle = 'ﺳﻨﺔ اﻟﺘﺨﺮﺝ';
                $eduValue = $app->graduation_year;
            }

            if ($app->onjop == 0) {
                $onjop = 'ﻧﻌﻢ';
            } else {
                $onjop = 'ﻻ';
            }

            if ($app->hRelatives == 2) {
                $hRelatives = 'ﻻ';
            } else {
                $hRelatives = 'ﻧﻌﻢ';
            }

            if ($app->howFind == 1) {
                $howFind = 'ﻣﻌﺎﺭﻑ';
            } elseif ($app->howFind == 2) {
                $howFind = 'اﻟﺘﺠﻮاﻝ';
            } elseif ($app->howFind == 3) {
                $howFind = 'ﺇﻋﻼﻥ';
            } else {
                $howFind = 'ﻣﻮﻗﻊ اﻟﺸﺮﻛﺔ';
            }

            if ($app->startwork == 0) {
                $startwork = 'ﺃﺳﺒﻮﻉ';
            } elseif ($app->startwork == 1) {
                $startwork = 'ﺃﺳﺒﻮﻋﻴﻦ';
            } elseif ($app->startwork == 2) {
                $startwork = 'ﺷﻬﺮ';
            } else {
                $startwork = 'ًﻓﻮﺭا';
            }

            $activeSheet->getStyle('A' . $i . ':H' . ($i + 40))->applyFromArray(['borders' => ['allBorders' => ['borderStyle' => 'thin', 'color' => ['rgb' => '000000']]]]);

            //Application Header
            $activeSheet->mergeCells('A' . $i . ':G' . ($i + 9));
            $activeSheet->setCellValue('A' . $i, $this->fixAr('ﻃـﻠــﺐ ﺗـﻮﻇـﻴــﻒ'));
            $activeSheet->getStyle('A' . $i . ':G' . ($i + 9))->applyFromArray(
                $header
            );
            $activeSheet->getRowDimension($i + 1)->setRowHeight(12.2);
            $activeSheet->getRowDimension($i + 2)->setRowHeight(12.2);
            $activeSheet->getRowDimension($i + 3)->setRowHeight(12.2);
            $activeSheet->getRowDimension($i + 4)->setRowHeight(12.2);
            $activeSheet->getRowDimension($i + 5)->setRowHeight(12.2);
            $activeSheet->getRowDimension($i + 6)->setRowHeight(12.2);
            $activeSheet->getRowDimension($i + 7)->setRowHeight(12.2);
            $activeSheet->getRowDimension($i + 8)->setRowHeight(12.2);
            $activeSheet->getRowDimension($i + 9)->setRowHeight(12.2);

            //Image
            $activeSheet->mergeCells('H' . $i . ':H' . ($i + 9));
            $activeSheet->getStyle('H' . $i . ':H' . ($i + 9))->applyFromArray($header);

            //Basic Information
            $activeSheet->mergeCells('A' . ($i + 10) . ':H' . ($i + 10));
            $activeSheet->setCellValue('A' . ($i + 10), $this->fixAr('اﻟﺒﻴﺎﻧﺎﺕ اﻟﺸﺨﺼﻴﺔ'));

            $activeSheet->setCellValue('A' . ($i + 11), $this->fixAr('ﺃﺳﻢ ﻣﻘﺪﻡ اﻟﻄﻠﺐ'));
            $activeSheet->mergeCells('B' . ($i + 11) . ':H' . ($i + 11));
            $activeSheet->setCellValue('B' . ($i + 11), $this->fixAr($app->name));
            $activeSheet->setCellValue('A' . ($i + 12), $this->fixAr('ﺗﺎﺭﻳﺦ اﻟﻤﻴﻼﺩ'));
            $activeSheet->mergeCells('B' . ($i + 12) . ':D' . ($i + 12));
            $activeSheet->setCellValue('B' . ($i + 12), $this->fixAr($app->b_date));
            $activeSheet->setCellValue('E' . ($i + 12), $this->fixAr('ﺗﺎﺭﻳﺦ اﻟﺘﻘﺪﻳﻢ'));
            $activeSheet->mergeCells('F' . ($i + 12) . ':H' . ($i + 12));
            $activeSheet->setCellValue('F' . ($i + 12), $this->fixAr($app->created_at));
            $activeSheet->setCellValue('A' . ($i + 13), $this->fixAr('اﻟﻌﻨﻮاﻥ اﻟﺤﺎﻟﻰ'));
            $activeSheet->mergeCells('B' . ($i + 13) . ':H' . ($i + 13));
            $activeSheet->setCellValue('B' . ($i + 13), $this->fixAr($app->inArea->name));
            $activeSheet->setCellValue('A' . ($i + 14), $this->fixAr('ﺭﻗﻢ اﻟﻤﻮﺑﺎﻳﻞ'));
            $activeSheet->mergeCells('B' . ($i + 14) . ':H' . ($i + 14));
            $activeSheet->setCellValue('B' . ($i + 14), $this->fixAr($mobile));
            $activeSheet->setCellValue('A' . ($i + 15), $this->fixAr('اﻟﺠﻨﺲ'));
            $activeSheet->mergeCells('B' . ($i + 15) . ':H' . ($i + 15));
            $activeSheet->setCellValue('B' . ($i + 15), $this->fixAr($gender));
            $activeSheet->setCellValue('A' . ($i + 16), $this->fixAr('اﻟﺤﺎﻟﺔ اﻻﺟﺘﻤﺎﻋﻴﺔ'));
            $activeSheet->mergeCells('B' . ($i + 16) . ':H' . ($i + 16));
            $activeSheet->setCellValue('B' . ($i + 16), $this->fixAr($marital));
            $activeSheet->setCellValue('A' . ($i + 17), $this->fixAr('اﻟﺨﺪﻣﺔ اﻟﻌﺴﻜﺮﻳﺔ'));
            $activeSheet->mergeCells('B' . ($i + 17) . ':H' . ($i + 17));
            $activeSheet->setCellValue('B' . ($i + 17), $this->fixAr($military));
            $activeSheet->setCellValue('A' . ($i + 18), $this->fixAr('ﺭﺧﺼﻪ اﻟﻘﻴﺎﺩﺓ'));
            $activeSheet->mergeCells('B' . ($i + 18) . ':H' . ($i + 18));
            $activeSheet->setCellValue('B' . ($i + 18), $this->fixAr($cdrive));

            //Edu Information
            $activeSheet->mergeCells('A' . ($i + 19) . ':H' . ($i + 19));
            $activeSheet->mergeCells('A' . ($i + 20) . ':C' . ($i + 20));
            $activeSheet->mergeCells('A' . ($i + 21) . ':C' . ($i + 21));
            $activeSheet->mergeCells('A' . ($i + 22) . ':C' . ($i + 22));
            $activeSheet->mergeCells('A' . ($i + 23) . ':C' . ($i + 23));

            $activeSheet->setCellValue('A' . ($i + 19), $this->fixAr('اﻟﺪﺭﺟﺔ اﻟﻌﻤﻠﻴﺔ '));
            $activeSheet->setCellValue('A' . ($i + 20), $this->fixAr('اﻟﺠﺎﻣﻌﺔ / اﻟﻤﻌﻬﺪ / اﻟﻤﺪﺭﺳﺔ'));
            $activeSheet->setCellValue('A' . ($i + 21), $this->fixAr('اﻟﻜﻠﻴﺔ / اﻟﺪﺭﺟﺔ اﻟﻌﻠﻤﻴﺔ'));
            $activeSheet->setCellValue('A' . ($i + 22), $this->fixAr($eduTitle) . ' ');
            $activeSheet->setCellValue('A' . ($i + 23), $this->fixAr('ﻣﻬﺎﺭاﺕ اﻟﺤﺎﺳﺐ اﻷﻟﻰ'));

            $activeSheet->mergeCells('D' . ($i + 20) . ':H' . ($i + 20));
            $activeSheet->mergeCells('D' . ($i + 21) . ':H' . ($i + 21));
            $activeSheet->mergeCells('D' . ($i + 22) . ':H' . ($i + 22));
            $activeSheet->mergeCells('D' . ($i + 23) . ':H' . ($i + 23));

            $activeSheet->setCellValue('D' . ($i + 20), $this->fixAr($app->university));
            $activeSheet->setCellValue('D' . ($i + 21), $this->fixAr($app->college));
            $activeSheet->setCellValue('D' . ($i + 22), $this->fixAr($eduValue));
            $activeSheet->setCellValue('D' . ($i + 23), $this->fixAr($cskills));

            $activeSheet->mergeCells('A' . ($i + 24) . ':H' . ($i + 24));
            $activeSheet->mergeCells('A' . ($i + 25) . ':B' . ($i + 26));
            $activeSheet->mergeCells('C' . ($i + 25) . ':D' . ($i + 26));
            $activeSheet->mergeCells('E' . ($i + 25) . ':F' . ($i + 26));
            $activeSheet->mergeCells('G' . ($i + 25) . ':G' . ($i + 26));
            $activeSheet->mergeCells('H' . ($i + 25) . ':H' . ($i + 26));

            $activeSheet->setCellValue('A' . ($i + 24), $this->fixAr('اﻟﺨﺒﺮاﺕ اﻟﺴﺎﺑﻘﺔ (ﻣﺒﺘﺪﺋﺎً ﺑﺄﺧﺮ ﻭﻇﻴﻔﺔ )'));
            $activeSheet->getStyle('A' . ($i + 24))->applyFromArray($header);

            $activeSheet->setCellValue('A' . ($i + 25), $this->fixAr('ﺃﺳﻢ اﻟﺠﻬﺔ'));
            $activeSheet->setCellValue('C' . ($i + 25), $this->fixAr('اﻟﻮﻇﻴﻔﺔ'));
            $activeSheet->setCellValue('E' . ($i + 25), $this->fixAr('ﻓﺘﺮﺓ اﻟﻌﻤﻞ'));
            $activeSheet->setCellValue('G' . ($i + 25), $this->fixAr('اﻟﺮاﺗﺐ'));
            $activeSheet->setCellValue('H' . ($i + 25), $this->fixAr('ﺳﺒﺐ ﺗﺮﻙ اﻟﻮﻇﻴﻔﺔ'));
            foreach ($app->experiences as $exp) {
                $activeSheet->getStyle('A' . ($i + 27) . ':H' . ($i + 27))
                    ->getAlignment()->setWrapText(true);
                $activeSheet->getRowDimension(($i + 27))->setRowHeight('-1');

                $time = $exp->time;

                //Work Exp
                $activeSheet->mergeCells('A' . ($i + 27) . ':B' . ($i + 27));
                $activeSheet->mergeCells('C' . ($i + 27) . ':D' . ($i + 27));
                $activeSheet->mergeCells('E' . ($i + 27) . ':F' . ($i + 27));
                $activeSheet->mergeCells('G' . ($i + 27) . ':G' . ($i + 27));
                $activeSheet->mergeCells('H' . ($i + 27) . ':H' . ($i + 27));

                $activeSheet->setCellValue('A' . ($i + 27), $this->fixAr($exp->company_name));
                $activeSheet->setCellValue('C' . ($i + 27), $this->fixAr($exp->jop_name));
                $activeSheet->setCellValue('E' . ($i + 27), $this->fixAr($time));
                $activeSheet->setCellValue('G' . ($i + 27), $this->fixAr($exp->salary));
                $activeSheet->setCellValue('H' . ($i + 27), $this->fixAr($exp->left_reson));
                $i++;
            }

            //Other Info
            $activeSheet->mergeCells('A' . (($i + 28) - 1) . ':D' . (($i + 28) - 1));
            $activeSheet->mergeCells('A' . (($i + 29) - 1) . ':D' . (($i + 29) - 1));
            $activeSheet->mergeCells('A' . (($i + 30) - 1) . ':D' . (($i + 30) - 1));
            $activeSheet->mergeCells('A' . (($i + 31) - 1) . ':D' . (($i + 31) - 1));
            $activeSheet->mergeCells('A' . (($i + 32) - 1) . ':D' . (($i + 32) - 1));
            $activeSheet->mergeCells('A' . (($i + 33) - 1) . ':D' . (($i + 33) - 1));
            $activeSheet->mergeCells('A' . (($i + 34) - 1) . ':E' . (($i + 34) - 1));

            $activeSheet->setCellValue('A' . (($i + 28) - 1), $this->fixAr('ﻫﻞ اﻧﺖ ﻣﻮﻇﻒ ﺣﺎﻟﻴﺎً؟'));
            $activeSheet->mergeCells('E' . (($i + 28) - 1) . ':H' . (($i + 28) - 1));
            $activeSheet->setCellValue('E' . (($i + 28) - 1), $this->fixAr($onjop));

            $activeSheet->setCellValue('A' . (($i + 29) - 1), $this->fixAr('ﻫﻞ اﻧﺖ ﻣﺆﻣﻦ ﻋﻠﻴﻚ ﺗﺎﻣﻴﻦ اﺟﺘﻤﺎﻋﻲ ﺣﺎﻟﻴﺎ ؟'));
            $activeSheet->mergeCells('E' . (($i + 29) - 1) . ':H' . (($i + 29) - 1));
            $activeSheet->setCellValue('E' . (($i + 29) - 1), $this->fixAr($haveInsurance));

            $activeSheet->setCellValue('A' . (($i + 30) - 1), $this->fixAr('ﻫﻞ ﺗﺤﺼﻞ ﻋﻠﻰ ﻣﻌﺎﺵ ﺣﻜﻮﻣﻰ ؟'));
            $activeSheet->mergeCells('E' . (($i + 30) - 1) . ':H' . (($i + 30) - 1));
            $activeSheet->setCellValue('E' . (($i + 30) - 1), $this->fixAr($havePension));

            $activeSheet->setCellValue('A' . (($i + 31) - 1), $this->fixAr('ﻫﻞ ﻟﺪﻳﻚ ﺃﻗﺎﺭﺏ ﻳﻌﻤﻠﻮﻥ ﺑﺎﻟﺸﺮﻛﺔ؟'));
            $activeSheet->setCellValue('E' . (($i + 31) - 1), $this->fixAr($hRelatives));
            if ($app->hRelatives == 2) {
                $activeSheet->mergeCells('F' . (($i + 31) - 1) . ':H' . (($i + 31) - 1));
            } else {
                $activeSheet->mergeCells('F' . (($i + 31) - 1) . ':G' . (($i + 31) - 1));
                $activeSheet->setCellValue('F' . (($i + 31) - 1), $this->fixAr('ﻣﻦ ﻓﻀﻠﻚ اﺫﻛﺮ اﻻﺳﻢ '));
                $activeSheet->setCellValue('H' . (($i + 31) - 1), $this->fixAr($app->hRelativesN));
            }
            $activeSheet->setCellValue('A' . (($i + 32) - 1), $this->fixAr('ﻛﻴﻒ ﻋﻠﻤﺖ ﻋﻦ ﺳﺎﺱ ﺇﻳﺠﻴﺒﺖ ؟'));
            $activeSheet->mergeCells('E' . (($i + 32) - 1) . ':H' . (($i + 32) - 1));
            $activeSheet->setCellValue('E' . (($i + 32) - 1), $this->fixAr($howFind));

            $activeSheet->setCellValue('A' . (($i + 33) - 1), $this->fixAr('ﻣﺘﻰ ﺗﺴﺘﻄﻴﻊ ﺑﺪء اﻟﻌﻤﻞ ﻓﻰ اﻟﺸﺮﻛﺔ ؟'));
            $activeSheet->mergeCells('E' . (($i + 33) - 1) . ':H' . (($i + 33) - 1));
            $activeSheet->setCellValue('E' . (($i + 33) - 1), $this->fixAr($startwork));

            $activeSheet->setCellValue('A' . (($i + 34) - 1), $this->fixAr('ﺃﻗﺮ ﺑﺄﻥ اﻟﻤﻌﻠﻮﻣﺎﺕ اﻟﻮاﺭﺩﻩ اﻋﻼﻩ ﺻﺤﻴﺤﺔ ﻭﺣﻘﻴﻘﻴﺔ'));
            $activeSheet->setCellValue('F' . (($i + 34) - 1), $this->fixAr('اﻟﺘﻮﻗﻴﻊ'));
            $activeSheet->mergeCells('G' . (($i + 34) - 1) . ':H' . (($i + 34) - 1));

            //Auth info
            $activeSheet->mergeCells('A' . (($i + 35) - 1) . ':H' . (($i + 35) - 1));
            $activeSheet->setCellValue('A' . (($i + 35) - 1), $this->fixAr('ﻓﻘﻂ ﻹﺳﺘﻌﻤﺎﻝ اﻹﺩاﺭﺓ'));
            $activeSheet->getStyle('A' . (($i + 35) - 1))->applyFromArray($header);

            $activeSheet->setCellValue('A' . (($i + 36) - 1), $this->fixAr('اﻟﺘﻘﻴﻴﻢ اﻟﻤﺒﺪﺋﻰ'));
            $activeSheet->mergeCells('B' . (($i + 36) - 1) . ':D' . (($i + 36) - 1));
            $activeSheet->setCellValue('B' . (($i + 36) - 1), $this->fixAr($app->Frate));
            $activeSheet->mergeCells('E' . (($i + 36) - 1) . ':F' . (($i + 36) - 1));
            $activeSheet->setCellValue('E' . (($i + 36) - 1), $this->fixAr('اﻟﻮﻇﻴﻔﺔ اﻟﻤﻘﺘﺮﺣﺔ'));
            $activeSheet->mergeCells('G' . (($i + 36) - 1) . ':H' . (($i + 36) - 1));
            $activeSheet->setCellValue('G' . (($i + 36) - 1), $this->fixAr($app->ExJop));

            $activeSheet->setCellValue('A' . (($i + 37) - 1), $this->fixAr('ﺣﺎﻟﺔ اﻟﻤﻘﺎﺑﻠﺔ'));
            $activeSheet->mergeCells('B' . (($i + 37) - 1) . ':D' . (($i + 37) - 1));
            $activeSheet->setCellValue('E' . (($i + 37) - 1), $this->fixAr('ﺑﻮاﺳﻄﺔ'));
            $activeSheet->setCellValue('G' . (($i + 37) - 1), $this->fixAr('ﺗﺎﺭﻳﺦ اﻟﻘﺒﻮﻝ'));
            $activeSheet->setCellValue('A' . (($i + 38) - 1), $this->fixAr('ﺃﺳﻢ اﻟﻤﺎﺭﻛﺔ'));
            $activeSheet->mergeCells('B' . (($i + 38) - 1) . ':D' . (($i + 38) - 1));
            $activeSheet->setCellValue('E' . (($i + 38) - 1), $this->fixAr('اﻟﻔﺮﻉ'));
            $activeSheet->setCellValue('G' . (($i + 38) - 1), $this->fixAr('اﻟﻤﺴﻤﻰ اﻟﻮﻇﻴﻔﻲ'));

            $activeSheet->mergeCells('A' . (($i + 39) - 1) . ':E' . (($i + 43) - 1));
            $activeSheet->setCellValue('A' . (($i + 39) - 1), $this->fixAr('ﺗﻮﻗﻴﻊ ﻣﺪﻳﺮ اﻟﻤﻮاﺭﺩ اﻟﺒﺸﺮﻳﺔ'));
            $activeSheet->getStyle('A' . (($i + 39) - 1))->applyFromArray($header);
            $activeSheet->mergeCells('F' . (($i + 39) - 1) . ':H' . (($i + 43) - 1));
            $activeSheet->setCellValue('F' . (($i + 39) - 1), $this->fixAr('ﺗﻮﻗﻴﻊ ﻣﺪﻳﺮ اﻟﻤﺎﺭﻛﺔ'));
            $activeSheet->getStyle('F' . (($i + 39) - 1))->applyFromArray($header);

            $activeSheet->setBreak('A' . ($i + 43), '1');
            $i = $i + 45;
        }
    }
}
