<?php

namespace App\Http\Controllers;

use App\Applicants\Area;
use App\Applicants\branch;
use App\DataTables\AreaDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Area  = new AreaDataTable();
        $Areas = Area::select('name', 'area_id');
        return $Area->with(['data' => $Areas])->render('Area.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $branchs = branch::all();
        return view('Area.create', compact('branchs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
        ]);
        $Area = Area::create([
            'name' => $request->name,
        ]);
        $Area->selection()->sync($request['branch']);

        Session::flash('flash_message', 'تم اضافة المنطقة بنجاح');

        return redirect(route('Area.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    public function branchIN($branchs, $selections)
    {
        foreach ($branchs as $keyb => $branch) {
            if (count($selections) >= 1) {
                foreach ($selections as $keys => $selection) {
                    if ($selection->id === $branch->id) {
                        $branchIN[$keyb]['name'] = $branch->name;
                        $branchIN[$keyb]['id']   = $branch->id;
                        $branchIN[$keyb]['avas'] = "1";
                        break;
                    } else {
                        $branchIN[$keyb]['name'] = $branch->name;
                        $branchIN[$keyb]['id']   = $branch->id;
                        $branchIN[$keyb]['avas'] = "0";
                    }
                }
            } else {
                $branchIN[$keyb]['name'] = $branch->name;
                $branchIN[$keyb]['id']   = $branch->id;
                $branchIN[$keyb]['avas'] = "0";}
        }
        return $branchIN;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Area    = Area::where('area_id', $id)->with('selection')->first();
        $branch  = branch::all();
        $branchs = $this->branchIN($branch, $Area->selection);
        return view('Area.edit', compact('Area', 'branchs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $value = $this->validate($request, [
            'area_id' => 'required|integer|max:255',
            'name'    => 'required|string|max:255',
        ]);
        Area::where('area_id', $value['area_id'])->update($value);

        $area = Area::where('area_id', $value['area_id'])->first();
        if (!empty($request['branch'])) {
            $area->selection()->sync($request['branch']);
        } else {
            $area->selection()->detach();
        }

        Session::flash('flash_message', 'تم تعديل بيانات المستخدم بنجاح');

        return redirect(route('Area.index'));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return "";
    }
}
