<?php

namespace App\Http\Controllers;

use App\Applicants\applicant;
use App\Applicants\Area;
use App\Applicants\branch;
use App\Applicants\experience;
use App\DataTables\applicantDataTable;
use Carbon\Carbon;
use Gumlet\ImageResize;
use Illuminate\Http\Request;

class ApplicantController extends Controller
{
    public $folder;
    protected $dbDebug = 0;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(applicantDataTable $app)
    {
        $areas   = Area::all();
        $branchs = branch::all();
        $apps    = applicant::with('inArea');
        return $app->with(['data' => $apps])->render('applicant.index', compact('areas', 'branchs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $areas = Area::all();
        return view('applicant.create', compact('areas'));
    }
    public function expValidate(Request $request)
    {
        $requests = array_filter($request->all());
        //Exp Array Validate
        $result = preg_grep('/^company_name(\d{1,4})/', array_keys($requests));
        for ($i = 0; $i < count($result); $i++) {
            $exp3[$i] = $this->validate($request,
                [
                    'company_name' . $i => 'nullable|string',
                    'jop_name' . $i     => 'nullable|string',
                    'time' . $i         => 'nullable|string',
                    'salary' . $i       => 'nullable|string',
                    'left_reson' . $i   => 'nullable|string',
                ]
            );
            array_walk($exp3[$i], function ($a, $b) use (&$exps, $i) {
                $exps[$i][substr($b, 0, -1)] = $a;
            });
        }
        if (!empty($exps)) {
            return $exps;
        } else {
            return false;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, applicant $applicant, experience $experience)
    {
        if ($this->dbDebug == 1) {
            \DB::enableQueryLog();
        }

        //Emp Validate
        $input                    = $this->validate($request, $this->applicantValidate());
        $input['b_date']          = date('Y-m-d H:i:s', strtotime($request->b_date));
        $input['college_year']    = ($request->edu == '1') ? 1 : $request->college_year;
        $input['graduation_year'] = ($request->edu == '0') ? 0 : $request->graduation_year;

        $exps      = $this->expValidate($request);
        $applicant = $applicant->Create($input);

        if (!empty($exps)) {
            foreach ($exps as $exp2) {
                $exp2['user_id'] = $applicant->id;
                $experience->Create($exp2);
            }
        }
        if ($this->dbDebug == 1) {
            $laQuery = \DB::getQueryLog();
            dd($laQuery);
            \DB::disableQueryLog();
        }

        return redirect(route('app.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    function print($id) {
        $applicant = applicant::find($id);
        return view('applicant.print', compact('applicant'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $applicant = applicant::with('experiences')->find($id);
        if ($applicant === null) {
            return redirect('/home');
        }

        $branchs = branch::all();
        $areas   = Area::all();
        $data    = $this->Data($applicant);
        return view('applicant.edit', compact('applicant', 'areas', 'branchs'))->with($data);
    }

    public function Data(applicant $app)
    {
        //Handel Data
        $name = $app->name;

        /* TODO:: JS Work */
        /* Profile Image */
        if (!file_exists(public_path($app->img)) || $app->img == null) {
            $ProfileImage = "noImage.png";
        } elseif ($app->img !== null) {
            $ProfileImage = $app->img;
        }
        $BirthOfDate = $app->b_date;
        $LivingArea  = $app->area;

        /* Mobile */
        if (!empty($app->mobile2)) {
            $MobileNumber2 = $app->mobile2;
        } else {
            $MobileNumber2 = '';
        }
        $MobileNumber = $app->mobile;

        /* TODO:: JS Work */
        /* Gender */
        if ($app->gender == '1') {
            $Gender = "ﺫﻛﺮ";
        } else {
            $Gender = "اﻧﺜﻲ";
        }
        /* TODO:: JS Work */
        /* Marital By Gender */
        $MaritalStatue = $app->marital;

        /* TODO:: JS Work */
        /* Military Statue */
        $MilitaryStatue = $app->military;

        /* TODO:: JS Work */
        /* Can Drive Statue */
        $CanDrive = $app->cdrive;

        $UniversityName = $app->university;
        $CollegeName    = $app->college;
        $Edu            = $app->edu;
        if ($Edu == 0) {
            $StudyYear = $app->college_year;
            $StudyName = "اﻟﻔﺮﻗﺔ";
        } elseif ($Edu == 1) {
            $StudyName = "ﺳﻨﺔ اﻟﺘﺨﺮﺝ ";
            $StudyYear = $app->graduation_year;
        }
        if ($app->cskills == 0) {
            $ComputerSkills = "ﻣﺒﺘﺪﻯء";
        } elseif ($app->cskills == 1) {
            $ComputerSkills = "ﻣﻘﺒﻮﻝ";
        } elseif ($app->cskills == 2) {
            $ComputerSkills = "ﺟﻴﺪ";
        } elseif ($app->cskills == 3) {
            $ComputerSkills = "ﺟﻴﺪ ﺟﺪا";
        } elseif ($app->cskills == 4) {
            $ComputerSkills = "ﻣﻤﺘﺎﺯ";
        }

        $Onjop         = ($app->onjop == 0) ? "ﻧﻌﻢ" : "ﻻ";
        $WhenStartWork = ($app->startwork == 0) ? "ﺃﺳﺒﻮﻉ" : ($app->startwork == 1) ? "ﺃﺳﺒﻮﻋﻴﻦ" : ($app->startwork == 2) ? "ﺷﻬﺮ" : ($app->startwork == 3) ? "ًﻓﻮﺭا" : null;
        $HaveInsurance = ($app->haveInsurance == 1) ? "ﻧﻌﻢ" : "ﻻ";
        $HavePension   = ($app->havePension == 1) ? "ﻧﻌﻢ" : "ﻻ";
        $HaveRelatives = ($app->hRelatives == 2) ? "ﻻ" : "ﻻ";
        $RelativesName = $app->hRelativesN;
        if ($app->howFind == 1) {
            $HowFindUs = "ﻣﻌﺎﺭﻑ";
        } elseif ($app->howFind == 2) {
            $HowFindUs = "اﻟﺘﺠﻮاﻝ";
        } elseif ($app->howFind == 3) {
            $HowFindUs = "اعلان / انترنت";
        } elseif ($app->howFind == 4) {
            $HowFindUs = "اعلان / جريدة";
        } elseif ($app->howFind == 5) {
            $HowFindUs = "ﻣﻮﻗﻊ اﻟﺸﺮﻛﺔ";
        }

        $ExpectedJop = $app->ExJop;
        $Fristrate   = $app->Frate;
        if ($app->interSat == "0") {
            $interSat = "ﻣﻮاﻓﻖ ﻋﻠﻴﻪ";
        } elseif ($app->interSat == "1") {
            $interSat = "ﻣﺮﻓﻮﺽ";
        } elseif ($app->interSat == "2") {
            $interSat = "اﻧﺘﻈﺎﺭ";
        }

        if ($app->interBy == "1") {
            $interBy = "ﻣﺤﻤﺪﺯﻫﺮﺓ";
        } elseif ($app->interBy == "2") {
            $interBy = "ﺣﺴﺎﻡﻋﺰﺕ";
        } elseif ($app->interBy == "3") {
            $interBy = "ﻫﺎﻧﻰﻋﺼﺎﻡ";
        } elseif ($app->interBy == "4") {
            $interBy = "ﻋﻤﺮﻭاﻟﺠﺰاﺭ";
        } elseif ($app->interBy == "5") {
            $interBy = "ﻫﺎﻧﻰﻣﺤﻤﺪ";
        } elseif ($app->interBy == "6") {
            $interBy = "ﻣﺆﻣﻦﻋﺒﺪاﻟﺮﺣﻤﻦ";
        }

        $interDate = $app->interDate;
        $JobBrand  = $app->JobBrand;

        if ($app->JobBranch == "1") {
            $JobBranch = "ﻋﺒﺎﺱ اﻟﻌﻘﺎﺩ";
        } elseif ($app->JobBranch == "2") {
            $JobBranch = "اﻟﺘﺠﻤﻊ";
        } elseif ($app->JobBranch == "3") {
            $JobBranch = "اﻟﻤﻬﻨﺪﺳﻴﻦ";
        } elseif ($app->JobBranch == "4") {
            $JobBranch = "ﻣﻮﻝ اﻟﻌﺮﺏ";
        } elseif ($app->JobBranch == "5") {
            $JobBranch = "ﻣﻮﻝ ﻣﺼﺮ";
        }

        $JobTitle  = $app->JobTitle;
        $CreatedAt = $app->created_at;
        return get_defined_vars();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, applicant $applicant)
    {
        echo "<pre>";
        //      dd($request);
        echo "</pre>";

        ini_set('post_max_size', '200M');
        ini_set('upload_max_filesize', '200M');

        $date         = new Carbon();
        $this->folder = 'uploads' . '/' . $date->year . '/' . $date->month;

        if ($this->dbDebug == 1) {
            \DB::enableQueryLog();
        }
        if ($request->hasFile('pPhoto')) {
            if (!file_exists(public_path($this->folder))) {
                \File::MakeDirectory(public_path($this->folder), 0755, true);
            }
            $file = \Input::file('pPhoto');

            $filename = md5(time()) . '.' . $file->getClientOriginalExtension();
            $imgPath  = $this->folder . '/' . $filename;

            $image = new ImageResize($file->getRealPath());
            $image->resizeToBestFit(150, 200);
            $image->save($imgPath);
        }
        if ($request->hasFile('cv')) {
            if (!file_exists(public_path($this->folder))) {
                \File::MakeDirectory(public_path($this->folder), 0755, true);
            }
            $cvName = time() . '.' . $request->cv->getClientOriginalExtension();
            $cvPath = $request->cv->move($this->folder, $cvName);
        }

        $appValidate = $this->validate($request, $this->applicantValidate());

        $appValidate['college_year']    = ($request->edu == '1') ? '1' : $request->college_year;
        $appValidate['graduation_year'] = ($request->edu == '0') ? '0' : $request->graduation_year;
        if (!empty($imgPath)) {
            $appValidate['img'] = $imgPath;
        } elseif (!empty($request['img']) and $request['img'] === 'remove') {
            $appValidate['img'] = "";
        }
        if (!empty($cvPath)) {
            $appValidate['cv'] = $cvPath;
        } elseif (!empty($request['cv']) and $request['cv'] === 'remove') {
            $appValidate['cv'] = "";
        }
        $applicant = $applicant->updateOrCreate(['id' => $id], $appValidate);
        //Exp Array Validate
        $result = preg_grep('/^company_name(\d{1,4})/', array_keys($request->all()));
        for ($i = 0; $i < count($result); $i++) {
            $exp3[$i] = $this->validate(
                $request,
                [
                    'expId' . $i        => 'sometimes|required',
                    'company_name' . $i => 'nullable|string',
                    'jop_name' . $i     => 'nullable|string',
                    'time' . $i         => 'nullable|string',
                    'salary' . $i       => 'nullable|string',
                    'left_reson' . $i   => 'nullable|string',
                ]
            );
            array_walk($exp3[$i], function ($a, $b) use (&$exps, $i) {
                $exps[$i][substr($b, 0, -1)] = $a;
            });
        }
        if (!empty($exps)) {
            foreach ($exps as $exp2) {
                if (!empty($exp2['expId'])) {
                    $experiences = $applicant->experiences()->updateOrCreate(['id' => $exp2['expId'], 'user_id' => $applicant->id], $exp2);
                } else {
                    $exp2['user_id'] = $applicant->id;
                    $experiences     = $applicant->experiences()->Create($exp2);
                }
            }
        }
        if ($this->dbDebug == 1) {
            $laQuery = \DB::getQueryLog();
            dd($laQuery);
            \DB::disableQueryLog();
        }

        return redirect(route('app.index'));
    }

    /**
     * Remove User Image.
     *
     * @param  int                      $id
     *
     * @return boolean
     */

    public function removeImage($id, applicant $applicant)
    {
        $applicant = $applicant->update(['id' => $id], ['img' => null]);
        var_dump($applicant);
    }

    /**
     * Backup View Control.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function backupView()
    {
        return view('applicant.backup');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $applicant = applicant::findOrFail($id);
        $applicant->delete();
        return "";
    }

    public function applicantValidate()
    {
        return [
            'name'            => 'required',
            'b_date'          => 'required',
            'mobile'          => 'required',
            'mobile2'         => 'nullable',
            'area'            => 'required',
            'gender'          => 'required',
            'military'        => 'required',
            'marital'         => 'required',
            'cdrive'          => 'required',
            'edu'             => 'required',
            'university'      => 'required',
            'college'         => 'required',
            'cskills'         => 'required',
            'onjop'           => 'required',
            'startwork'       => 'required',
            'havePension'     => 'required',
            'howFind'         => 'required',
            'haveInsurance'   => 'required',
            'graduation_year' => 'nullable|max:4',
            'college_year'    => 'nullable|numeric|min:1|max:5',
            'hRelatives'      => 'required',
            'hRelativesN'     => 'nullable',
            'ExJop'           => 'required',
            'Frate'           => 'required',
            'notes'           => 'nullable',
            'img'             => 'nullable',
            'CV'              => 'nullable',
            'interSat'        => 'nullable',
            'interBy'         => 'nullable',
            'interDate'       => 'nullable',
            'JobBrand'        => 'nullable',
            'JobTitle'        => 'nullable',
            'JobBranch'       => 'nullable',
            'apply'           => 'required',
        ];
    }

}
