<?php

namespace App\Applicants;

use App\Applicants\Area;
use Illuminate\Database\Eloquent\Model;

class branch extends Model
{
    protected $table = "branch";

    protected $hidden = ['pivot'];

    public function selection()
    {
        return $this->belongsToMany(Area::class, 'branch_area', 'branch_id', 'area_id');
    }
}
