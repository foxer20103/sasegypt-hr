<?php

namespace App\Applicants;

use Illuminate\Database\Eloquent\Model;

class applicant extends Model
{
    public $table = 'applicants';

    protected $fillable = ['id', 'name', 'b_date', 'mobile', 'mobile2', 'area', 'gender', 'military', 'marital', 'cdrive', 'university', 'college', 'cskills', 'onjop', 'startwork', 'havePension', 'howFind', 'haveInsurance', 'graduation_year', 'college_year', 'hRelatives', 'hRelativesN', 'ExJop', 'Frate', 'edu', 'img', 'notes', 'cv', "interSat", "interBy", "interDate", "JobBrand", "JobBranch", "JobTitle", 'apply'];
    /* Relations */
    public function experiences()
    {
        return $this->hasMany(experience::class, 'user_id');
    }

    public function inArea()
    {
        return $this->hasOne(Area::class, 'area_id', 'area');
    }
    public function getCreatedAtAttribute($value)
    {
        return date('Y-m-d', strtotime($value));
    }
}
