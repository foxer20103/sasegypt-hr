<?php

namespace App\Applicants;

use App\Applicants\branch;
use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    public $table         = 'areas';
    protected $fillable   = ['name'];
    protected $primaryKey = 'area_id';
    protected $hidden     = ['pivot'];

    public function applicants()
    {
        return $this->belongsTo(applicant::class, 'area', $this->primaryKey);
    }

    public function selection()
    {
        return $this->belongsToMany(Branch::class, 'branch_area', 'area_id', 'branch_id');
    }
}
