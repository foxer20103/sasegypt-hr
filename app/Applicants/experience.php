<?php

namespace App\Applicants;

use Illuminate\Database\Eloquent\Model;

class experience extends Model
{
    public $table = 'experiences';

    protected $fillable = ['id', 'expCat', 'expCat', 'company_name', 'jop_name', 'time', 'salary', 'left_reson', 'user_id'];

    public $timestamps = false;

    public function applicants()
    {
        return $this->hasMany(applicant::class, 'user_id', 'id');
    }

    public function getTimeFromAttribute($value)
    {
        return date('Y-m', strtotime($value));
    }

    public function getTimeToAttribute($value)
    {
        return date('Y-m', strtotime($value));
    }

}
