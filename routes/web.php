<?php
Auth::routes();

Route::group(['middleware' => ['web', 'auth']], function () {
    Route::get('/', function () {return redirect(route('app.index'));});

    Route::resource('user', 'User\UsersController')->middleware('roles:admin');
    Route::resource('Area', 'AreaController')->middleware('roles:write,admin');
    Route::resource('app', 'ApplicantController');

    Route::get('app/{id}/print', 'ApplicantController@print')->name('app.print');
    Route::get('app/{id}/printer', function ($id) {
        $header    = view('applicant.backup.header')->render();
        $applicant = App\Applicants\applicant::with('inArea', 'experiences')->find($id);
        $backup    = new App\Http\Controllers\backup\backup();
        $data      = $backup->GetData($applicant);
        $view      = view('applicant.backup.printer')->with($data)->render();
        $footer    = '</body></html>';
        return $header . $view . $footer;
    })->name('app.printer');
    Route::get('app/{id}/printer2', function ($id) {
        $applicant = App\Applicants\applicant::with('inArea', 'experiences')->find($id);
        $backup    = new App\Http\Controllers\backup\backup();
        $data      = $backup->GetData($applicant);
        return view('applicant.backup.printer2')->with($data)->render();
    })->name('app.printer2');
    Route::get('app/{id}/download', 'backup\backup@download')->name('app.download')->middleware('roles:backup,admin');
    Route::post('backup', 'backup\backup@backupProcess')->middleware('roles:backup,admin');
    Route::get('home', 'ApplicantController@index')->name('home');
    Route::get('check', 'ApplicantController@check')->name('check');
    Route::get('search', 'SearchController@filter')->name('search');
    Route::get('backup', 'ApplicantController@backupView')->name('app.backup')->middleware('roles:backup,admin');
});
Route::get('migrate', function () {
    Artisan::call('migrate');
});
