@extends('layouts.userApp')
@section('title','Log in')
@section('content')
	<!-- /.login-logo -->
	<div class="login-box-body">
		<p class="login-box-msg">Sign in to start your session</p>
		<form method="POST" action="{{ route('login') }}">
			{{ csrf_field() }} @if ($errors->has('email'))
				<span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span> @endif
			<div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
				<label class="control-label"></label>
				<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
				<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
			</div>
			@if ($errors->has('password'))
				<span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span> @endif
			<div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
				<label class="control-label"></label>
				<input id="password" type="password" class="form-control" name="password" required>
				<span class="glyphicon glyphicon-lock form-control-feedback" aria-hidden="true"></span>
			</div>
			<div class="row">
				<div class="col-xs-8">
					<div class="checkbox icheck">
						<label>
							<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
						</label>
					</div>
				</div>
				<!-- /.col -->
				<div class="col-xs-4">
					<button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
				</div>
				<!-- /.col -->
			</div>
		</form>
		<a href="{{route('password.request')}}">I forgot my password</a><br>
	</div>
	<!-- /.login-box-body -->
@endsection