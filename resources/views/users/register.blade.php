@extends('layouts.app')
@section('title','Add New User')
@section('content')
<div class="col-sm-11">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">اﻧﺸﺎء مستخدم جديد</h3>
        </div>
        @include('layouts.message')
        <form class="form-horizontal" method="POST" action="{{ route('user.store') }}">
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name" class="col-md-4 control-label">الاسم</label>

                <div class="col-md-6">
                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col-md-4 control-label">البريد الالكترونى</label>

                <div class="col-md-6">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="col-md-4 control-label">كلمة المرور</label>

                <div class="col-md-6">
                    <input id="password" type="password" class="form-control" name="password" required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label for="password-confirm" class="col-md-4 control-label">تاكيد كلمة المرور</label>

                <div class="col-md-6">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                </div>
            </div>
            <div class="form-group">
                <label for="Roule" class="col-md-4 control-label">الصلحيات</label>

                <div class="col-md-6" id="Roule">
                    <label class="checkbox-inline">
                      <input type="checkbox" name="Roule[]" value="1">Write / Edit
                    </label>
                    <label class="checkbox-inline">
                      <input type="checkbox" name="Roule[]" value="2">Delete
                    </label>
                    <label class="checkbox-inline">
                      <input type="checkbox" name="Roule[]" value="3">Backup
                    </label>
                    <label class="checkbox-inline">
                      <input type="checkbox" class="admin" name="Roule[]" value="4">Admin
                    </label>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        حفظ
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
