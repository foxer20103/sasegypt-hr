@extends('layouts.app')
@section('title','Edit User')
@section('jsSection')
<script type="text/javascript">
    $(function(){
        $(".admin").click(function(){
            $('#Roule input:checkbox').not(this).prop('checked', this.checked);
        });
    });
</script>
@endsection
@section('content')
<div class="col-sm-11">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">تعديل مستخدم </h3>
        </div>
        @include('layouts.message')
        <form class="form-horizontal" method="POST" action="{{ route('user.update',$user->id) }}">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <input type="hidden" name="id" value="{{ $user->id }}">
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name" class="col-md-4 control-label">الاسم</label>

                <div class="col-md-6">
                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name',$user->name) }}" required autofocus>

                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col-md-4 control-label">البريد الالكترونى</label>

                <div class="col-md-6">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email',$user->email) }}" required>

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="col-md-4 control-label">كلمة المرور</label>

                <div class="col-md-6">
                    <input id="password" type="password" value="" class="form-control" name="password" >

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label for="password-confirm" class="col-md-4 control-label">تاكيد كلمة المرور</label>

                <div class="col-md-6">
                    <input id="password-confirm" type="password" class="form-control" value="" name="password_confirmation" >
                </div>
            </div>
            <div class="form-group">
                <label for="Roule" class="col-md-4 control-label">الصلحيات</label>

                <div class="col-md-6" id="Roule">
                    <label class="checkbox-inline">
                      <input type="checkbox" name="Roule[]" {{$user->hasRole('write') ? 'checked' : '' }} value="1">Write / Edit
                    </label>
                    <label class="checkbox-inline">
                      <input type="checkbox" name="Roule[]" {{$user->hasRole('delete') ? 'checked' : '' }} value="2">Delete
                    </label>
                    <label class="checkbox-inline">
                      <input type="checkbox" name="Roule[]" {{$user->hasRole('backup') ? 'checked' : '' }} value="3">Backup
                    </label>
                    <label class="checkbox-inline">
                      <input type="checkbox" class="admin" {{$user->hasRole('admin') ? 'checked' : '' }} name="Roule[]" value="4">Admin
                    </label>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        حفظ
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
