@extends('layouts.app')
@section('title','Edit Area')
@section('jsSection')
@endsection
@section('content')
<div class="col-sm-11">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">تعديل منطقة</h3>
        </div>
        @include('layouts.message')
        <form class="form-horizontal" method="POST" action="{{ route('Area.update',$Area->area_id) }}">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <input type="hidden" name="area_id" value="{{ $Area->area_id }}">
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name" class="col-md-4 control-label">الاسم</label>

                <div class="col-md-6">
                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name',$Area->name) }}" required autofocus>

                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label for="Roule" class="col-md-4 control-label">الفروع</label>

                <div class="col-md-6" id="Roule">
                    @foreach ($branchs as $name => $branch)
                        <label class="checkbox-inline">
                          <input type="checkbox" name="branch[]" <?php if ($branch['avas'] === "1") {echo 'checked="checked"';}?> value="{{$branch['id']}}">{{$branch['name']}}
                        </label>
                    @endforeach
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        حفظ
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
