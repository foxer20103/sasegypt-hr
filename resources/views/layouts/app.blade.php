<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>{{config('app.name')}} - @yield('title')</title>
	<!-- Bootstrap Core CSS -->
	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
	<!-- MetisMenu CSS -->
	<link href="{{asset('css/plugins/metisMenu/metisMenu.min.css')}}" rel="stylesheet">
	<!-- Timeline CSS -->
	<link href="{{asset('css/plugins/timeline.css')}}" rel="stylesheet">
	<!-- Custom CSS -->
	<link href="{{asset('css/sb-admin-2.css')}}" rel="stylesheet">
	<!-- Custom Fonts -->
	<link href="{{asset('css/font-awesome/css/fontawesome-all.min.css')}}" rel="stylesheet" type="text/css">
@section('cssSection')
@show
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
-	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
<div id="wrapper">
	<!-- Navigation -->
	<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="{{route('home')}}">{{config('app.name')}}</a>
		</div>
		<!-- /.navbar-header -->
		<ul class="nav navbar-top-links navbar-left">
			<!-- /.dropdown -->
			<li class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">
					{{Auth::user()->name}}
					<i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
				</a>
				<ul class="dropdown-menu dropdown-user">
					<li>
						<a href="{{ route('logout') }}"
						   onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i
									class="fa fa-sign-out fa-fw"></i> Logout</a>
					</li>
					<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
						{{ csrf_field() }}
					</form>
				</ul>
				<!-- /.dropdown-user -->
			</li>
			<!-- /.dropdown -->
		</ul>

		<!-- /.navbar-top-links -->
		<div class="navbar-default sidebar" role="navigation">
			<div class="sidebar-nav navbar-collapse">
				<ul class="nav" id="side-menu">
					<li align="center">
						<img src="{{asset('logo.jpeg')}}" style="height: 150px; width: 125px;" alt="Logo">
					</li>
					@if(user()->hasRole('admin'))
					<li>
			          <a href="#">
			            <i class="fa fa-user fa-fw"></i> <span>Users Control</span>
			          </a>
			          <ul class="nav">
			            <li>
			            	<a href="{{route('user.index')}}">
			            		<i class="fa fa-plus-square fa-fw"></i> DashBoard
			            	</a>
			            </li>
			            <li class="active">
			            	<a href="{{route('user.create')}}">
			            		<i class="fa fa-plus-square fa-fw"></i> Add User
			            	</a>
			            </li>
			          </ul>
			        </li>
			        @endif
					@if(user()->hasAnyRole(['admin','write']))
					<li>
			          <a href="#">
			            <i class="fa fa-home fa-fw"></i> <span>Area Control</span>
			          </a>
			          <ul class="nav">
			            <li>
			            	<a href="{{route('Area.index')}}">
			            		<i class="fa fa-plus-square fa-fw"></i> DashBoard
			            	</a>
			            </li>
			            <li class="active">
			            	<a href="{{route('Area.create')}}">
			            		<i class="fa fa-plus-square fa-fw"></i> Add Area
			            	</a>
			            </li>
			          </ul>
			        </li>
			        @endif
			        <li>
						<a href="#">
							<i class="fa fa-home fa-fw"></i> <span>Applicant Control</span>
						</a>
						<ul class="nav">
							<li>
								<a href="{{route('home')}}"><i class="fa fa-tachometer-alt fa-fw"></i> Dashboard</a>
							</li>
							@if(user()->hasAnyRole(['admin','write']))
							<li>
								<a href="{{route('app.create')}}"><i class="fa fa-plus fa-fw"></i> Add Applicant</a>
							</li>
							@endif
							@if(user()->hasAnyRole(['admin','backup']))
							<li>
								<a href="{{route('app.backup')}}"><i class="fa fa-cloud-download-alt fa-fw"></i> Backup</a>
							</li>
							@endif
						</ul>
			        </li>
				</ul>
			</div>
			<!-- /.sidebar-collapse -->
		</div>

		<!-- /.navbar-static-side -->
	</nav>
	<!-- Page Content -->
	<div id="page-wrapper">
		<div class="row">

		@section('content') @show
		<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
	</div>
	<!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery Version 1.11.0 -->
<script src="{{asset('js/jquery-1.11.0.js')}}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="{{asset('js/metisMenu/metisMenu.min.js')}}"></script>
<!-- Morris Charts JavaScript -->
<script src="{{asset('js/raphael/raphael.min.js')}}"></script>
<script src="{{asset('js/morris/morris.min.js')}}"></script>
<!-- Custom Theme JavaScript -->
<script src="{{asset('js/sb-admin-2.js')}}"></script>

@section('jsSection')
@show
</body>
</html>
