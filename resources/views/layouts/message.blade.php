	@if(count($errors) > 0)
		@foreach($errors->all() as $error)
			<p class="alert alert-danger">{{ $error }}</p>
		@endforeach
	@endif
	@if(Session::has('flash_message'))
		<div class="alert alert-success">
			{{ Session::get('flash_message') }}
			<script type="text/javascript">
			setTimeout(function(){
				$("div.alert-success").remove();
			}, 5000 );
			</script>
		</div>
	@endif
