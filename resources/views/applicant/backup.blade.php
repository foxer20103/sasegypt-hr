@extends('layouts.app')
@section('title','Add Application')

@section('content')
	<div class="col-md-11">
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">انشاء نسخة احتياطية</h3>
			</div>
			@if(count($errors) > 0)
				@foreach($errors->all() as $error)
					<p class="alert alert-danger">{{ $error }}</p>
				@endforeach
			@endif
			<form class="form-horizontal" method="post" action="{{route('app.backup')}}">
			{{ csrf_field() }}
			<!-- /CSRFS -->
				<div class="form-group">
					<label for="FDate" class="col-md-4 control-label">بداية التاريخ :</label>
					<div class="col-md-5">
						<input class="form-control" id="FDate" name="FDate" type="date" min="2018-01-01" max="{{date('Y-m-d')}}" value="2018-01-01" />
					</div>
				</div>
				<!-- /Education Section -->
				<div class="form-group">
					<label for="EDate" class="col-md-4 control-label">نهاية التاريخ :</label>
					<div class="col-md-5">
						<input class="form-control" id="EDate" name="EDate" type="date" min="2018-01-01" max="{{date('Y-m-d')}}" value="{{date('Y-m-d')}}" />
					</div>
				</div>
				<!-- /Education Section -->
				<!-- /.box-footer -->
				<div class="box-footer pull-left">
					<button type="submit" name="type" value="xlsx" class="btn btn-info ">Excel</button>
					<button type="submit" name="type" value="pdf" class="btn btn-info ">Pdf</button>
					<button type="submit" name="type" value="db" class="btn btn-info ">DataBase</button>
				</div>
			</form>
		</div>
	</div>
	<!-- /.box -->
@endsection
