@extends('layouts.app')
@section('title','Edit Application')
@section('cssSection')
 <!-- bootstrap datepicker -->
 <link rel="stylesheet" href="{{asset('plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
 <!-- Select2 -->
 <link rel="stylesheet" href="{{asset('plugins/select2/select2.min.css')}}"/>
 <!-- DataTables -->
 <link rel="stylesheet" href="{{asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
 <!-- DataTables -->
 <link rel="stylesheet" href="{{asset('css/style.css')}}">
 <style>
  table#example {
   table-layout: fixed;
  }

  #example td {
   overflow: hidden;
   text-overflow: ellipsis;
  }

  #example .form-control {
   width: 100%;
  }

 </style>
 <!--
 gender
 edu
 -->
@endsection
@section('jsSection')
 <!-- InputMask -->
 <script src="{{asset('plugins/input-mask/jquery.inputmask.js')}}"></script>
 <script src="{{asset('plugins/input-mask/jquery.inputmask.date.extensions.js')}}"></script>
 <!-- Select2 -->
 <script src="{{asset('plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>
 <script src="{{asset('plugins/select2/select2.min.js')}}"></script>
 <!-- page script -->
 <script src="{{asset('js/script.js')}}"></script>
 <!-- DataTables -->
 <script src="{{asset('plugins/datatables.net/js/jquery.dataTables.min.js')}}"></script>
 <script src="{{asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
 <script>
  $(function () {
   $(document).on('change', 'input[name="cv"]', function() {
   var input = $(this),
   numFiles = input.get(0).files ? input.get(0).files.length : 1,
   label  = input.val().replace(/\\/g, '/').replace(/.*\//,'');
   input.trigger('fileselect', [numFiles, label]);
  });
   $(':file').on('fileselect', function(event, numFiles, label) {
    var input = $(this).parents('.input-group').find(':text'),
   log = numFiles > 1 ? numFiles + ' files selected' : label;
   if( input.length ) {
    input.val(log);
   }
  });
   /** Exp Table */
   var table = $('#example').DataTable({
    'ordering': false,
    "dom": '<"wrapper"t>',
   });
   var i = $('#example tbody tr >td input[type="hidden"]').length;

   $('#example tbody').on('click', 'button.row-remove', function () {
    table.row($(this).parents('tr')).remove().draw();
   });
   $('a#add_row').click(function () {
    table.row.add([
     '<input name="company_name'+i+'" type="text" value="{{old("company_name'+i +' ")}}" placeholder="اﺳﻢ ﺟﻬﺔ اﻟﻌﻤﻞ" class="form-control"/>',
    '<input name="jop_name'+i+'" type="text" value="{{old("jop_name'+i+'")}}" placeholder="اﻟﻮﻇﻴﻔﺔ" class="form-control"/>',
    '<input name="time'+i+'" type="text" value="{{old("time'+i+'")}}" class="form-control" />',
    '<input name="salary'+i+'" type="text" value="{{old("salary'+i+'")}}" placeholder="اﻟﻤﺮﺗﺐ اﻻﺳﺎﺳﻰ" class="form-control"/>',
     '<input name="left_reson'+i+'" type="text" value="{{old("left_reson'+i +'")}}" placeholder="اﺳﺒﺎﺏ ﺗﺮﻙ اﻟﻌﻤﻞ" class="form-control"/>',
     '<button class="btn btn-danger glyphicon glyphicon-remove row-remove"></button>',
    ]).draw(false);
    i++;
   });
   /** Gender Selection */

   /* gender */
 @if($applicant->gender==1)
   $("#gMale").attr('checked', true);
   $("#gfemale").attr('checked', false);
   $('#opMale,#military').css('display', 'block');
   $('#opFemale').css('display', 'none');
   $('#gMale').parents('.btn-group').find('label.btn').removeClass('active');
   if ($('#gMale').prop('checked')) {
    $('#gMale').parents('label.btn').addClass('active');
   }
 @else
   $("#gfemale").attr('checked', true);
   $("#gMale").attr('checked', false);
   $('#opMale,#military').css('display', 'none');
   $('#opFemale').css('display', 'block');

   $('#gfemale').parents('.btn-group').find('label.btn').removeClass('active');
   if ($('#gfemale').prop('checked')) {
    $('#gfemale').parents('label.btn').addClass('active');
   }
 @endif
   function readURL(input) {
    if (input.files && input.files[0]) {
     $('.img').css('display', 'block');
     var reader = new FileReader();

     reader.onload = function (e) {
      $('#img').attr('src', e.target.result);
     }

     reader.readAsDataURL(input.files[0]);
    }
   }

   $("input[name='pPhoto']").change(function () {
    readURL(this);
   });
   //Remove Image
   $('button#removeImg').click(function (e) {
    // confirm then
    e.preventDefault();
    if (confirm('هل تريد حذف الصورة الشخصية؟')) {
      $('form').append('<input type="hidden" name="img" value="remove"/>');
      $('div.img > img').attr('src','../../noImage.png');
    }else{
        alert("تم الغاء الصورة");
    }
   });
   //Remove Cv
   $('button#removeCv').click(function (e) {
    // confirm then
    e.preventDefault();
    if (confirm('هل تريد حذف السيرة الذاتية')) {
      $('form').append('<input type="hidden" name="cv" value="remove"/>');
      $('div.cv > a').remove();
    }else{
        alert("تم الغاء السيرة الذاتية");
    }
   });
     });
 </script>
@endsection
@section('content')
 <div class="col-sm-12">
  <div class="box box-info">
   <div class="box-header with-border">
    <h3 class="box-title">ﺗﻌﺪﻳﻞ ﻃﻠﺐ ﺗﻮﻇﻴﻒ</h3>
   </div>
      @include('layouts.message')
   <form class="form-horizontal" method="post" action="{{route('app.update',$applicant->id)}}"
      enctype="multipart/form-data">
   {{ csrf_field() }}
   <!-- /CSRFS -->
   {{ method_field('PUT') }}
   <!-- /PUT Method -->
    <div class="box-body">
     <div class="box-header with-border">
      <p class="h4 box-title ">
       <u>اﻟﺒﻴﺎﻧﺎﺕ اﻻﺳﺎﺳﻴﺔ:</u>
      </p>
     </div>
     <div class="form-group">
      <!-- Name -->
      <div class="col-sm-4 col-sm-offset-2 img">
       <img src="{{url($ProfileImage)}}" alt="Your Image" id="img" width="200" height="250">
       @if(!empty($applicant->img))
       <button id="removeImg">X</button>
       @endif
      </div>
     </div>
     <div class="form-group">
      <label for="appName" class="col-sm-2 control-label">اﻟﺼﻮﺭﺓ اﻟﺸﺨﺼﻴﺔ</label>
      <div class="col-sm-4">
       <label class="btn btn-default btn-file">
        Browse <input type="file" name="pPhoto" style="display: none;">
       </label>
      </div>
     </div>
     <div class="form-group">
      <!-- Name -->
      <label for="appName" class="col-sm-2 control-label">اﻻﺳﻢ</label>
      <div class="col-sm-4">
       <input type="text" value="{{old('name',$applicant->name)}}" class="form-control" id="appName" name="name"
           placeholder="اﺩﺧﻞ اﺳﻤﻚ ﺭﺑﺎﻋﻰ" required>
      </div>
      <!-- Birth Date -->
      <label for="dmask" class="col-sm-3 control-label">ﺗﺎﺭﻳﺦ اﻟﻤﻴﻼﺩ:</label>
      <div class="col-sm-3">
       <input type="text" value="{{old('b_date',$applicant->b_date)}}" id="dmask" class="form-control" name="b_date"
           placeholder="اﺩﺧﻞ ﺗﺎﺭﻳﺦ ﻣﻲﻻﺩﻙ" dir="ltr" required>
      </div>
     </div>
     <!-- /Name & B.Date -->
     <div class="form-group">
      <label for="mmask" class="col-sm-2 control-label">ﺭﻗﻢ اﻟﻤﻮﺑﺎﻳﻞ:</label>
      <div class="col-sm-4">
       <input type="text" value="{{old('mobile',$applicant->mobile)}}" id="mmask" class="form-control" name="mobile"
           placeholder="اﺩﺧﻞ ﺭﻗﻢ اﻟﻤﻮﺑﺎﻳﻞ" dir="ltr" required>
      </div>
      <label for="mobile2" class="col-sm-3 control-label">ﺭﻗﻢ اﻟﻤﻮﺑﺎﻳﻞ:</label>
      <div class="col-sm-3">
       <input type="text" value="{{old('mobile2',$applicant->mobile2)}}" id="mmask2" class="form-control" name="mobile2" placeholder="اﺩﺧﻞ ﺭﻗﻢ اﻟﻤﻮﺑﺎﻳﻞ" dir="ltr">
      </div>
     </div>
     <!-- /Mobiles -->
     <div class="form-group">
      <label for="area" class="col-sm-2 control-label">اﻟﻤﻨﻄﻘﺔ اﻟﺴﻜﻨﻴﺔ:</label>
      <div class="col-sm-4">
       <select id="area" name="area" class="form-control select2" required>
        <option selected disabled>Select Your Area</option>
        @foreach ($areas as $area)
         <option value="{{$area['area_id']}}"
           @if($LivingArea==$area['area_id']) selected @endif>{{$area['name']}}</option>
        @endforeach
              </select>
      </div>
     </div>
     <!-- /Area -->
     <div class="form-group">
      <label class="col-sm-2 control-label">اﻟﺠﻨﺲ:</label>
      <div class="col-sm-4">
       <div class="btn-group" role="group">
        <label class="btn btn-default form-label active">
         <input checked="checked" type="radio" value="1" name="gender" class="radio_btn" required>
         <i class="fa fa-male"></i>ﺫﻛﺮ
        </label>
        <label class="btn btn-default form-label">
         <input type="radio" value="0" name="gender" class="radio_btn">
         <i class="fa fa-female"></i>ﺃﻧﺜﻰ
        </label>
       </div>
      </div>
      <!-- \Sex -->
      <label for="military" class="col-sm-3 control-label">اﻟﺤﺎﻟﺔ اﻻﺟﺘﻤﺎﻋﻴﺔ:</label>
      <div class="col-sm-3">
       <select name="marital" id="opMale" class="form-control" style="width: 100%;" required>
        <option value="0" @if($applicant->marital=="0") selected @endif>اﻋﺰﺏ</option>
        <option value="1" @if($applicant->marital=="1") selected @endif>ﻣﺘﺰﻭﺝ</option>
        <option value="2" @if($applicant->marital=="2") selected @endif>ﻣﻄﻠﻖ</option>
        <option value="3" @if($applicant->marital=="3") selected @endif>ﺧﺎﻃﺐ</option>
       </select>
       <select name="marital" id="opFemale" class="form-control" style="width: 100%;" required>
        <option value="0" @if($applicant->marital=="0") selected @endif>اﻧﺴﺔ</option>
        <option value="1" @if($applicant->marital=="1") selected @endif>ﻣﺘﺰﻭﺟﺔ</option>
        <option value="2" @if($applicant->marital=="2") selected @endif>ﻣﻄﻠﻘﺔ</option>
        <option value="3" @if($applicant->marital=="3") selected @endif>ﻣﺨﻄﻮﺑﺔ</option>
       </select>
      </div>
     </div>
     <!-- /Sex & Marital  -->
     <div class="form-group">
      <label for="military" id="military" class="col-sm-2 control-label">اﻟﺨﺪﻣﺔ اﻟﻌﺴﻜﺮﻳﺔ:</label>
      <div class="col-sm-10" id="military">
       <div class="radio-inline">
        <input type="radio" name="military" id="military" value="0" @if($applicant->military=="0") checked="checked"
            @endif required> اﺩﻯ اﻟﺨﺪﻣﺔ
       </div>
       <div class="radio-inline">
        <input type="radio" name="military" id="optionsRadios2" @if($applicant->military=="1") checked="checked"
            @endif value="1"> اﻋﻔﺎء ﻣﺆﻗﺖ
       </div>
       <div class="radio-inline">
        <input type="radio" name="military" id="optionsRadios2" @if($applicant->military=="2") checked="checked"
            @endif value="2">اﻋﻔﺎء ﻧﻬﺎﺋﻰ
       </div>
       <div class="radio-inline">
        <input type="radio" name="military" id="optionsRadios2" @if($applicant->military=="3") checked="checked"
            @endif value="3">ﻟﻢ ﻳﺼﺒﻪ اﻟﺪﻭﺭ
       </div>
      </div>
      <!-- /Miltry Service -->
      <label for="cDrive" class="col-sm-2 control-label">ﺭﺧﺼﺔ اﻟﻘﻴﺎﺩﺓ:</label>
      <div class="col-sm-5">
       <div class="btn-group" role="group">
        <label class="btn btn-default form-label @if($applicant->cdrive=="1") active @endif">
         <input type="radio" value="1" @if($applicant->cdrive=="1") checked="checked" @endif name="cdrive"
             class="radio_btn" required>ﻧﻌﻢ
        </label>
        <label class="btn btn-default form-label @if($applicant->cdrive=="0") active @endif">
         <input type="radio" value="0" @if($applicant->cdrive=="0") checked="checked" @endif name="cdrive"
             class="radio_btn">ﻻ
        </label>
       </div>
      </div>
      <!-- /Miltry Service -->
      <label for="cDrive" class="col-sm-2 control-label">تاريخ الاضافة:</label>
      <div class="col-sm-3">
       <div class="btn-group" role="group">
        <input type="text" value="{{$applicant->updated_at}}" class="form-control" style="direction: ltr;">
       </div>
      </div>
    </div>
     <!-- /Drive Lenses Service -->
     <!-- /Miltry Service & Drive Lenses Service -->
     <div class="box-header with-border">
      <p class="h4 box-title">
       <u>اﻟﺪﺭﺟﺔ اﻟﻌﻠﻤﻴﺔ:</u>
      </p>
     </div>
     <div class="edu">
      <!-- Edu -->
      <div class="form-group">
       <div class="col-xs-10">
        <div class="btn-group" role="group">
         <label class="btn btn-default form-label @if($applicant->edu=="1") active @endif">
          <input id="cDrive" type="radio" value="1" @if($applicant->edu=="1") checked="checked" @endif name="edu"
              class="radio_btn" required>ﺧﺮﻳﺞ
         </label>
         <label class="btn btn-default form-label @if($applicant->edu=="0") active @endif">
          <input id="cDrive" type="radio" value="0" @if($applicant->edu=="0") checked="checked" @endif name="edu"
              class="radio_btn">ﻃﺎﻟﺐ
         </label>
        </div>
       </div>
      </div>
      <div class="form-group">
       <label for="university" class="col-sm-3 control-label">اﺳﻢ اﻟﺠﺎﻣﻌﺔ / اﻟﻤﻌﻬﺪ / اﻟﻤﺪﺭﺳﺔ</label>
       <div class="col-sm-6">
        <input type="text" value="{{old('university',$applicant->university)}}" id="university" name="university"
            class="form-control" required
            placeholder="اﺩﺧﻞ اﺳﻢ اﺧﺮ ﻣﻜﺎﻥ ﺩﺭﺳﺖ ﺑﻪ">
       </div>
      </div>
      <div class="form-group">
       <label for="college" class="col-sm-3 control-label">اﻟﺪﺭﺟﺔ اﻟﻌﻠﻤﻴﺔ</label>
       <div class="col-sm-6">
        <input type="text" value="{{old('college',$applicant->college)}}" id="college" name="college"
            class="form-control" required
            placeholder="اﺩﺧﻞ اﺳﻢ اﺧﺮ ﺩﺭﺟﺔ ﻋﻠﻤﻴﺔ ﺣﺼﻠﺖ ﻋﻠﻴﻬﺎ">
       </div>
      </div>
      <div class="form-group graduation_year" @if($applicant->edu=="1") style="display: block;"
        @else style="display: none;" @endif>
       <label for="graduation_year" class="col-sm-3 control-label">ﺳﻨﺔ اﻟﺘﺨﺮﺝ</label>
       <div class="col-sm-6">
        <input type="text" value="{{old('graduation_year',$applicant->graduation_year)}}" id="graduation_year"
            name="graduation_year" class="form-control" @if($applicant->edu=="1") required
            @endif placeholder="اﺩﺧﻞ ﺳﻨﺔ اﻟﺤﺼﻮﻝ ﻋﻠﻴﻬﺎ">
       </div>
      </div>
      <div class="form-group college-year" @if($applicant->edu=="0") style="display: block;"
        @else style="display: none;" @endif>
       <label for="college-year" class="col-sm-3 control-label">اﻟﻔﺮﻗﺔ اﻟﺪﺭاﺳﻴﺔ</label>
       <div class="col-sm-6">
        <input type="number" min="1" max="5" value="{{old('college_year',$applicant->college_year)}}" id="college-year"
            name="college_year" class="form-control" @if($applicant->edu=="0") required
            @endif placeholder="اﺩﺧﻞ اﻟﻔﺮﻗﺔ اﻟﺪﺭاﺳﻴﺔ">
       </div>
      </div>
     </div>
     <!-- /Education Section -->
     <div class="form-group">
      <label for="military" class="col-sm-3 control-label">ﻣﻬﺎﺭاﺕ اﻟﺤﺎﺳﺐ اﻻﻟﻰ:</label>
      <div class="col-sm-6">
       <select name="cskills" class="form-control" style="width: 100%;" required>
        <option value="0" @if($applicant->cskills=="0") selected @endif>ﻣﺒﺘﺪﻯء</option>
        <option value="1" @if($applicant->cskills=="1") selected @endif>ﻣﻘﺒﻮﻝ</option>
        <option value="2" @if($applicant->cskills=="2") selected @endif>ﺟﻴﺪ</option>
        <option value="3" @if($applicant->cskills=="3") selected @endif>ﺟﻴﺪ ﺟﺪا</option>
        <option value="4" @if($applicant->cskills=="4") selected @endif>ﻣﻤﺘﺎﺯ</option>
       </select>
      </div>
     </div>
     <!-- /Computer Skills -->
     <div class="box-header with-border">
      <p class="h4 box-title">
       <u>اﻟﺨﺒﺮﺓ اﻟﻌﻤﻠﻴﺔ:</u>
      </p>
     </div>
     <div class="form-group">
      <a id="add_row" class="col-sm-2 btn btn-default pull-right">اﺿﺎﻓﺔ ﺧﺒﺮﺓ</a>
     </div>
     <div class="form-group">
      <table id="example" class="table table-bordered table-hover">
       <thead>
       <tr>
        <th class="text-center">ﺇﺳﻢ ﺟﻬﺔ اﻟﻌﻤﻞ</th>
        <th class="text-center">اﻟﻮﻇﻴﻔﺔ</th>
        <th class="text-center">ﻓﺘﺮﺓ اﻟﻌﻤﻞ</th>
        <th class="text-center">اﻟﻤﺮﺗﺐ اﻷﺳﺎﺳﻰ</th>
        <th class="text-center">ﺃﺳﺒﺎﺏ ﺗﺮﻙ اﻟﻌﻤﻞ</th>
        <th class="text-center remove_width">ﺣﺬﻑ ﺧﺒﺮﺓ</th>
       </tr>
       </thead>
       <tbody>
       <?php $i = '0';?>
       @foreach($applicant->experiences as $exp)
        <tr role="row" class="odd">
         <td>
          <input type="hidden" name="expId{{$i}}" value="{{$exp->id}}">
          <input name="company_name{{$i}}" type="text" value="{{$exp->company_name}}" placeholder="اﺳﻢ ﺟﻬﺔ اﻟﻌﻤﻞ"
              class="form-control">
         </td>
         <td>
          <input name="jop_name{{$i}}" type="text" value="{{$exp->jop_name}}" placeholder="اﻟﻮﻇﻴﻔﺔ"
              class="form-control">
         </td>
         <td>
          <input name="time{{$i}}" type="text" value="{{$exp->time}}" placeholder="اﻟﻤﺮﺗﺐ اﻻﺳﺎﺳﻰ" class="form-control">
         </td>
         <td><input name="salary{{$i}}" type="text" value="{{$exp->salary}}" placeholder="اﻟﻤﺮﺗﺐ اﻻﺳﺎﺳﻰ"
              class="form-control"></td>
         <td><input name="left_reson{{$i}}" type="text" value="{{$exp->left_reson}}" placeholder="اﺳﺒﺎﺏ ﺗﺮﻙ اﻟﻌﻤﻞ"
              class="form-control"></td>
         <td>
          <button class="btn btn-danger glyphicon glyphicon-remove row-remove"></button>
         </td>
        </tr>
       <?php
$i++;
?>
       @endforeach
       </tbody>
      </table>
     </div>
     <!-- /Exp Table -->
     <div class="form-group">
      <label for="onjop" class="col-sm-4 control-label">ﻫﻞ اﻧﺖ ﻣﻮﻇﻒ ﺣﺎﻟﻴﺎً؟ </label>
      <div class="col-sm-5">
       <select name="onjop" id="onjop" class="form-control" style="width: 100%;" required>
        <option value="0" @if($applicant->onjop=="0") selected @endif>ﻧﻌﻢ</option>
        <option value="1" @if($applicant->onjop=="1") selected @endif>ﻻ</option>
       </select>
      </div>
     </div>
     <!-- /Computer Skills -->
     <div class="form-group">
      <label for="startwork" class="col-sm-4 control-label">ﻣﺘﻰ ﺗﺴﺘﻄﻴﻊ ﺑﺪء اﻟﻌﻤﻞ ﻓﻰ اﻟﺸﺮﻛﺔ ؟ </label>
      <div class="col-sm-5">
       <select name="startwork" id="startwork" class="form-control" style="width: 100%;" required>
        <option value="0" @if($applicant->startwork=="0") selected @endif>ﺃﺳﺒﻮﻉ</option>
        <option value="1" @if($applicant->startwork=="1") selected @endif>اﺳﺒﻮﻋﻴﻦ</option>
        <option value="2" @if($applicant->startwork=="2") selected @endif>ﺷﻬﺮ</option>
        <option value="3" @if($applicant->startwork=="3") selected @endif>ﻓﻮﺭا</option>
       </select>
      </div>
     </div>
     <!-- /Computer Skills -->
     <div class="form-group">
      <label for="haveInsurance" class="col-sm-4 control-label">ﻫﻞ اﻧﺖ ﻣﺆﻣﻦ ﻋﻠﻴﻚ ﺗﺎﻣﻴﻦ اﺟﺘﻤﺎﻋﻲ ﺣﺎﻟﻴﺎ ؟ </label>
      <div class="col-sm-5">
       <select name="haveInsurance" id="haveInsurance" class="form-control" style="width: 100%;" required>
        <option value="1" @if($applicant->haveInsurance=="1") selected @endif>ﻧﻌﻢ</option>
        <option value="2" @if($applicant->haveInsurance=="2") selected @endif>ﻻ</option>
       </select>
      </div>
     </div>
     <!-- /Computer Skills -->
     <div class="form-group">
      <label for="havePension" class="col-sm-4 control-label">ﻫﻞ ﺗﺤﺼﻞ ﻋﻠﻰ ﻣﻌﺎﺵ ﺣﻜﻮﻣﻰ ؟ </label>
      <div class="col-sm-5">
       <select name="havePension" id="havePension" class="form-control" style="width: 100%;" required>
        <option value="1" @if($applicant->havePension=="1") selected @endif>ﻧﻌﻢ</option>
        <option value="2" @if($applicant->havePension=="2") selected @endif>ﻻ</option>
       </select>
      </div>
     </div>
     <!-- /Have Pension -->
     <div class="form-group">
      <label for="mmask" class="col-sm-4 control-label">ﻫﻞ ﻟﺪﻳﻚ ﺃﻗﺎﺭﺏ ﻳﻌﻤﻠﻮﻥ ﺑﺎﻟﺸﺮﻛﺔ؟</label>
      <div class="col-sm-3">
       <select name="hRelatives" id="hRelatives" class="form-control" required>
        <option value="1" @if($applicant->hRelatives=="1") selected @endif>ﻧﻌﻢ</option>
        <option value="2" @if($applicant->hRelatives=="2") selected @endif>ﻻ</option>
       </select>
      </div>
      <!-- Have Relatives -->
      @if($applicant->hRelatives=="1")
       <div class="hRelativesN" style="display: block;">
        <label for="mmask" class="col-sm-2 control-label">ﻣﻦ ﻓﻀﻠﻚ اﺫﻛﺮ اﻻﺳﻢ:</label>
        <div class="col-sm-3">
         <input type="text" value="{{old('hRelativesN',$applicant->hRelativesN)}}" class="form-control"
             name="hRelativesN"
             placeholder="ﻣﻦ ﻓﻀﻠﻚ اﺫﻛﺮ اﻻﺳﻢ">
        </div>
       @endif
       <!-- Relatives Name -->
       </div>
     </div>
     <!-- /Relatives -->
     <div class="form-group">
      <label for="howFind" class="col-sm-4 control-label">ﻛﻴﻒ ﻋﻠﻤﺖ ﻋﻦ ﺳﺎﺱ ﺇﻳﺠﻴﺒﺖ ؟ </label>
      <div class="col-sm-5">
       <select name="howFind" id="howFind" class="form-control" style="width: 100%;" required>
        <option value="1" @if($applicant->howFind=="1") selected @endif>ﺃﺻﺪﻗﺎء / ﺃﻗﺎﺭﺏ</option>
        <option value="2" @if($applicant->howFind=="2") selected @endif>اﺛﻨﺎء اﻟﺘﺠﻮاﻝ</option>
        <option value="3" @if($applicant->howFind=="3") selected @endif>اعلان / انترنت</option>
        <option value="3" @if($applicant->howFind=="4") selected @endif>اعلان / جريدة</option>
        <option value="4" @if($applicant->howFind=="5") selected @endif>ﻣﻮﻗﻊ اﻟﺸﺮﻛﺔ</option>
       </select>
      </div>
     </div>
     <!-- /How Find Us -->
     <div class="form-group">
      <label for="ExJop" class="col-sm-4 control-label">اﻟﻮﻇﻴﻔﺔ اﻟﻤﺤﺘﻤﻠﺔ</label>
      <div class="col-sm-3">
       <select name="ExJop" id="ExJop" class="form-control" style="width: 100%;" required>
        <option @if($applicant->ExJop=="Sales") selected @endif>Sales</option>
        <option @if($applicant->ExJop=="Stock") selected @endif>Stock</option>
        <option @if($applicant->ExJop=="Security Fitting") selected @endif>Security Fitting</option>
        <option @if($applicant->ExJop=="Casher") selected @endif>Casher</option>
        <option @if($applicant->ExJop=="VM") selected @endif>VM</option>
        <option @if($applicant->ExJop=="Assist") selected @endif>Assist</option>
        <option @if($applicant->ExJop=="Manger") selected @endif>Manger</option>
       </select>
      </div>
      <label for="Frate" class="col-sm-3 control-label">اﻟﺘﻘﻴﻴﻢ اﻟﻤﺒﺪﺋﻰ</label>
      <div class="col-sm-3">
       <select name="Frate" id="Frate" class="form-control" required>
        <option @if($applicant->Frate=="1") selected @endif>1</option>
        <option @if($applicant->Frate=="2") selected @endif>2</option>
        <option @if($applicant->Frate=="3") selected @endif>3</option>
        <option @if($applicant->Frate=="4") selected @endif>4</option>
        <option @if($applicant->Frate=="5") selected @endif>5</option>
        <option @if($applicant->Frate=="6") selected @endif>6</option>
        <option @if($applicant->Frate=="7") selected @endif>7</option>
        <option @if($applicant->Frate=="8") selected @endif>8</option>
        <option @if($applicant->Frate=="9") selected @endif>9</option>
        <option @if($applicant->Frate=="10") selected @endif>10</option>
       </select>
      </div>
     </div>
     <!-- /Expected Jop -->
     <div class="form-group">
      <label for="notes" class="col-sm-4 control-label">ﻣﻼﺣﻈﺎﺕ</label>
      <div class="col-sm-5">
       <textarea name="notes" id="notes" cols="50" rows="5">{{old('notes',$applicant->notes)}}</textarea>
      </div>
     </div>
      <!-- /Expected Jop -->
      <div class="form-group">
        <label for="apply" class="col-sm-4 control-label">التقديم</label>
        <div class="col-sm-3">
          <select name="apply" id="apply" class="form-control" required>
            <option @if($applicant->apply=="الادارة") selected @endif>الادارة</option>
            <option @if($applicant->apply=="الفروع") selected @endif>الفروع</option>
          </select>
        </div>
      </div>
     <!-- /Education Section -->
     <div class="form-group cv">
      <label for="appName" class="col-sm-2 control-label">اﻟﺴﻴﺮﺓ اﻟﺰاﺗﻴﺔ</label>
      <div class="col-lg-6 col-sm-6 col-12">
       <div class="input-group">
        <label class="input-group-btn">
         <span class="btn btn-primary">
          Browse&hellip; <input name="cv" type="file" style="display: none;" multiple>
         </span>
        </label>
        <input type="text" class="form-control" readonly>
       </div>
       <span class="help-block">CV Or Imgs</span>
      </div>
     <a href="\{{$applicant->cv}}">{{$applicant->cv}}</a>
     @if(!empty($applicant->cv))
        <button id="removeCv">X</button>
     @endif
     </div>
     <div class="box-header with-border">
      <p class="h4 box-title">
       <u>ﺣﺎﻟﺔ اﻟﻄﻠﺐ:</u>
      </p>
     </div>
     <!-- /Sex & Marital  -->
     <div class="form-group">
      <label for="interview" id="interview" class="col-sm-2 control-label">ﺣﺎﻟﺔ اﻟﻤﻘﺎﺑﻠﺔ:</label>
      <div class="col-sm-4" id="interview">
       <div class="radio-inline">
        <input type="radio" name="interSat" id="interview" value="0" @if($applicant->interSat=="0") checked="checked"
            @endif> ﻣﻮاﻓﻖ ﻋﻠﻴﻪ
       </div>
       <div class="radio-inline">
        <input type="radio" name="interSat" id="interview" @if($applicant->interSat=="1") checked="checked"
            @endif value="1"> ﻣﺮﻓﻮﺽ
       </div>
       <div class="radio-inline">
        <input type="radio" name="interSat" id="interview" @if($applicant->interSat=="2") checked="checked"
            @endif value="2">اﻧﺘﻈﺎﺭ
       </div>
       <div class="radio-inline">
        <input type="radio" name="interSat" id="interview" @if($applicant->interSat=="3") checked="checked"
            @endif value="3">ترك العمل
       </div>
       </div>
       <label for="interviewBY" class="col-sm-2 control-label">ﺗﻤﺖ اﻟﻤﻘﺎﺑﻠﺔ ﺑﻮﺳﺎﻃﺔ:</label>
       <div class="col-sm-4">
       <select id="interBy" name="interBy" class="form-control select2" required>
        <option selected disabled>اﺳﻢ اﻟﻤﺪﻳﺮ</option>
         <option value="1" @if($applicant->interBy=='1') selected @endif>ﻣﺤﻤﺪ ﺯﻫﺮﺓ</option>
         <option value="2" @if($applicant->interBy=='2') selected @endif>ﺣﺴﺎﻡ ﻋﺰﺕ</option>
         <option value="3" @if($applicant->interBy=='3') selected @endif>ﻫﺎﻧﻰ ﻋﺼﺎﻡ</option>
         <option value="4" @if($applicant->interBy=='4') selected @endif>ﻋﻤﺮﻭ اﻟﺠﺰاﺭ</option>
         <option value="5" @if($applicant->interBy=='5') selected @endif>ﻫﺎﻧﻰ ﻣﺤﻤﺪ</option>
         <option value="6" @if($applicant->interBy=='6') selected @endif>ﻣﺆﻣﻦ ﻋﺒﺪ اﻟﺮﺣﻤﻦ</option>
       </select>
        </div>
       </div>
      <div class="form-group">
      <label for="interviewDate" class="col-sm-2 control-label">ﺗﺎﺭﻳﺦ اﻟﻘﺒﻮﻝ:</label>
      <div class="col-sm-4">
      <input type="date" class="form-control" name="interDate" id="interviewDate" value="{{old('interDate',$applicant->interDate)}}"  placeholder="اﺩﺧﻞ اﻻﺳﻢ">
       </div>
       <label for="JobBrand" class="col-sm-2 control-label">اﺳﻢ اﻟﻤﺎﺭﻛﺔ:</label>
       <div class="col-sm-4">
       <select id="JobBrand" name="JobBrand" class="form-control select2" required>
        <option selected disabled>ﺃﺳﻢ اﻟﻤﺎﺭﻛﺔ</option>
         <option @if($applicant->JobBrand=='Terranova') selected @endif>Terranova</option>
         <option @if($applicant->JobBrand=='Calliope') selected @endif>Calliope</option>
       </select>
       </div>
       </div>
      <div class="form-group">
               <label for="JobBranch" class="col-sm-2 control-label">اﻟﻔﺮﻉ:</label>
       <div class="col-sm-4">
       <select id="JobBranch" name="JobBranch" class="form-control select2" required>
        <option selected disabled>ﻣﻜﺎﻥ اﻟﻔﺮﻉ</option>
                @foreach ($branchs as $branch)
         <option value="{{$branch['id']}}"
           @if($applicant->JobBranch==$branch['id']) selected @endif>{{$branch['name']}}</option>
        @endforeach
       </select>
       </div>
      <label for="JobBranch" class="col-sm-2 control-label">اﻟﻤﺴﻤﻰ اﻟﻮﻇﻴﻔﻲ :</label>
      <div class="col-sm-4">
       <select id="JobTitle" name="JobTitle" class="form-control select2" required>
      <option  @if($applicant->JobTitle=='Sales') selected @endif>Sales</option>
      <option  @if($applicant->JobTitle=='Stock') selected @endif>Stock</option>
      <option  @if($applicant->JobTitle=='Security Fitting') selected @endif>Security Fitting</option>
      <option  @if($applicant->JobTitle=='Casher') selected @endif>Casher</option>
      <option  @if($applicant->JobTitle=='VM') selected @endif>VM</option>
      <option  @if($applicant->JobTitle=='Assist') selected @endif>Assist</option>
      <option  @if($applicant->JobTitle=='Manger') selected @endif>Manger</option>
    </select>
       </div>
       </div>
    </div>
    <!-- /.box-footer -->
    <div class="box-footer pull-left">
     <button type="reset" class="btn btn-default">اﻟﻐﺎء</button>
     <button type="submit" class="btn btn-info ">ﺣﻔﻆ</button>
    </div>
   </form>
  </div>
 </div>
 <!-- /.box -->
@endsection