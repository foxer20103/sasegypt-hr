<!DOCTYPE html>
<html dir="rtl">
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="{{asset('css/style.css')}}">
	<title>Print Page</title>
	<style>
		.sign {font-size: 18px}
	</style>
</head>
<body class="A4">
<div class="sheet">
	<table class="text" id="printTable">
		<!-- Header Section -->
	 <?php
if (!empty($applicant->experiences)) {
    $height = (165 - ((count($applicant->experiences) - 1) * 15));
} else {
    $height = 165;
}
?>
		<tr style="height: {{$height}}pt">
			<td colspan="8" style="text-align: center;border: 0; font-size:28px">ﻧﻤﻮﺫﺝ ﻃﻠﺐ ﻭﻇﻴﻔﺔ</td>
			<td colspan="2" style="border:0;border-right: 1px solid #000;">
				@if(!empty($applicant->img))
				<img style="max-height:{{$height}}pt;min-height: 100%;width: 100%;height: auto;" src="@if(!empty($applicant->img)){{asset($applicant->img)}}@else#@endif" alt="alt">
				@endif
			</td>
		</tr>
		<tr>
			<td colspan="10" class="h1" style="border-right: 1px solid #000">اﻟﺒﻴﺎﻧﺎﺕ اﻟﺸﺨﺼﻴﺔ</td>
		</tr>
		<!-- \Header Section -->
		<tr>
			<td class="noBorder">ﺃﺳﻢ ﻣﻘﺪﻡ اﻟﻄﻠﺐ:</td>
			<td class="noBorder" colspan="9">{{$applicant->name}}</td>
		</tr>
		<tr>
			<td class="noBorder">ﺗﺎﺭﻳﺦ اﻟﻤﻴﻼﺩ :</td>
			<td class="noBorder" colspan="4">{{$applicant->b_date}}</td>
			<td class="noBorder" colspan="1">تاريخ التقديم:</td>
			<td class="noBorder" colspan="4">{{$applicant->created_at}}</td>
		</tr>
		<tr>
			<td class="noBorder">اﻟﻌﻨﻮاﻥ اﻟﺤﺎﻟﻰ :</td>
			<td class="noBorder" colspan="9">{{$applicant->inArea->name}}</td>
		</tr>
		<tr>
			<td class="noBorder">ﺭﻗﻢ اﻟﻤﻮﺑﺎﻳﻞ :</td>
			<td class="noBorder" colspan="9">
				{{$applicant->mobile}} @if(!empty($applicant->mobile2)) / {{$applicant->mobile2}} @endif
			</td>
		</tr>
		<tr>
			<td class="noBorder">اﻟﺠﻨﺲ :</td>
			<td class="noBorder @if($applicant->gender==1) sign @else unsign @endif">ﺫﻛﺮ</td>
			<td class="noBorder"></td>
			<td class="noBorder @if($applicant->gender==0) sign @else unsign @endif">اﻧﺜﻰ</td>
			<td class="noBorder" colspan="6"></td>
		</tr>
		<tr>
			<td class="noBorder">اﻟﺤﺎﻟﺔ الاﺟﺘﻤﺎﻋﻴﺔ :</td>
			<td class="noBorder @if($applicant->marital==0) sign @else unsign @endif" colspan="2">@if($applicant->gender==1)
					ﺃﻋﺰﺏ@elseﺃﻧﺴﺔ@endif</td>
			<td class="noBorder @if($applicant->marital==1) sign @else unsign @endif" colspan="2">@if($applicant->gender==1)
					ﻣﺘﺰﻭﺝ@elseﻣﺘﺰﻭﺟﺔ@endif</td>
			<td class="noBorder @if($applicant->marital==2) sign @else unsign @endif" colspan="2">@if($applicant->gender==1)
					ﻣﻄﻠﻖ@elseﻣﻄﻠﻘﺔ@endif</td>
			<td class="noBorder @if($applicant->marital==3) sign @else unsign @endif" colspan="3">@if($applicant->gender==1)
					خاطب@elseمخطوبة@endif</td>
		</tr>
		@if($applicant->gender==1)
			<tr class="noBorder">
				<td class="noBorder">اﻟﺨﺪﻣﺔ اﻟﻌﺴﻜﺮﻳﺔ :</td>
				<td class="noBorder @if($applicant->military==0) sign @else unsign @endif" colspan="2">ﺃﺩﻯ اﻟﺨﺪﻣﺔ</td>
				<td class="noBorder @if($applicant->military==1) sign @else unsign @endif" colspan="2">ﺇﻋﻔﺎء ﻣﺆﻗﺖ</td>
				<td class="noBorder @if($applicant->military==2) sign @else unsign @endif" colspan="2">ﺇﻋﻔﺎء ﻧﻬﺎﺋﻰ</td>
				<td class="noBorder @if($applicant->military==3) sign @else unsign @endif" colspan="3">ﻟﻢ ﻳﺼﺒﻪ اﻟﺪﻭﺭ</td>
			</tr>
		@endif
		<tr>
			<td class="noBorder">ﺭﺧﺼﻪ اﻟﻘﻴﺎﺩﺓ :</td>
			<td class="noBorder @if($applicant->cdrive==1)sign @else unsign @endif">ﻧﻌﻢ</td>
			<td class="noBorder @if($applicant->cdrive==0)sign @else unsign @endif">لا</td>
			<td class="noBorder" colspan="7"></td>
		</tr>
		<!-- \basic Section -->
			<tr>
				<td colspan="10" class="h1 upper-border">اﻟﺪﺭﺟﺔ اﻟﻌﻤﻠﻴﺔ :</td>
			</tr>
			<tr>
				<td>اﻟﺠﺎﻣﻌﺔ / اﻟﻤﻌﻬﺪ / اﻟﻤﺪﺭﺳﺔ</td>
				<td colspan="9" style="padding-right:20px;">{{$applicant->university}}</td>
			</tr>
			<tr>
				<td>اﻟﻜﻠﻴﺔ / اﻟﺪﺭﺟﺔ اﻟﻌﻠﻤﻴﺔ</td>
				<td colspan="9" style="padding-right:20px;">{{$applicant->college}}</td>
			</tr>
			<tr>
				@if ($applicant->edu==0)
					<td>الفرقة</td>
					<td colspan="9" style="padding-right:20px;">{{$applicant->college_year}}</td>
				@else
					<td>سنة التخرج</td>
					<td colspan="9" style="padding-right:20px;">{{$applicant->graduation_year}}</td>
				@endif
			</tr>
		<!-- \Education Section -->
		<tr>
			<td colspan="10" class="h1 noBorder">ﻣﻬﺎﺭاﺕ اﻟﺤﺎﺳﺐ اﻷﻟﻰ :</td>
		</tr>
		<tr style="text-align: center">
			<td class="noBorder @if($applicant->cskills==0) sign @else unsign @endif" colspan="2">ﻣﺒﺘﺪﻯء</td>
			<td class="noBorder @if($applicant->cskills==1) sign @else unsign @endif" colspan="2">ﻣﻘﺒﻮﻝ</td>
			<td class="noBorder @if($applicant->cskills==2) sign @else unsign @endif" colspan="2">ﺟﻴﺪ</td>
			<td class="noBorder @if($applicant->cskills==3) sign @else unsign @endif" colspan="2">جيد جدا</td>
			<td class="noBorder @if($applicant->cskills==4) sign @else unsign @endif" colspan="2">ﻣﻤﺘﺎﺯ</td>
		</tr>
		<!-- \Computer Section -->
		<tr>
			<td colspan="10" class="h1">اﻟﺨﺒﺮﺓ اﻟﻌﻤﻠﻴﺔ :</td>
		</tr>
			<tr style="text-align: center">
				<td>ﺇﺳﻢ ﺟﻬﺔ اﻟﻌﻤﻞ</td>
				<td>اﻟﻮﻇﻴﻔﺔ</td>
				<td colspan="4">ﻓﺘﺮﺓ اﻟﻌﻤﻞ</td>
				<td>اﻟﻤﺮﺗﺐ اﻷﺳﺎﺳﻰ</td>
				<td colspan="3">ﺃﺳﺒﺎﺏ ﺗﺮﻙ اﻟﻌﻤﻞ</td>
			</tr>

			@if(!empty($applicant->experiences['0']))
				@foreach($applicant->experiences as $exp)
				<tr style="text-align:center">
					<td>{{$exp->company_name}}</td>
					<td>{{$exp->jop_name}}</td>
					<td colspan="4">{{$exp->time}}</td>
					<td>{{$exp->salary}}</td>
					<td colspan="3">{{$exp->left_reson}}</td>
				</tr>
			@endforeach
			@else
				<tr style="text-align:center">
					<td colspan="10">ﻵ توجد خبرة لهذا الشخص</td>
				</tr>
			@endif
		<!-- \Experines Section -->
			<tr>
				<td class="noBorder" colspan="2">ﻫﻞ اﻧﺖ ﻣﻮﻇﻒ ﺣﺎﻟﻴﺎً؟</td>
				<td class="noBorder @if($applicant->onjop==0)sign @else unsign @endif" colspan="2">ﻧﻌﻢ</td>
				<td class="noBorder @if($applicant->onjop==1)sign @else unsign @endif" colspan="6">لا</td>
			</tr>
			<tr>
				<td class="noBorder" colspan="2">ﻫﻞ اﻧﺖ ﻣﺆﻣﻦ ﻋﻠﻴﻚ ﺗﺎﻣﻴﻦ اﺟﺘﻤﺎﻋﻲ ﺣﺎﻟﻴﺎ ؟</td>
				<td class="noBorder @if($applicant->haveInsurance==1) sign @else unsign @endif" colspan="2">ﻧﻌﻢ</td>
				<td class="noBorder @if($applicant->haveInsurance==2) sign @else unsign @endif" colspan="6">لا</td>
			</tr>
			<tr>
				<td class="noBorder" colspan="2">ﻫﻞ ﺗﺤﺼﻞ ﻋﻠﻰ ﻣﻌﺎﺵ ﺣﻜﻮﻣﻰ ؟</td>
				<td class="noBorder @if($applicant->havePension==1) sign @else unsign @endif" colspan="2">ﻧﻌﻢ</td>
				<td class="noBorder @if($applicant->havePension==2) sign @else unsign @endif" colspan="6">لا</td>
			</tr>
			<tr>
				<td class="noBorder" colspan="2">ﻫﻞ ﻟﺪﻳﻚ ﺃﻗﺎﺭﺏ ﻳﻌﻤﻠﻮﻥ ﺑﺎﻟﺸﺮﻛﺔ؟</td>
				@if($applicant->hRelatives==2)
					<td class="noBorder unsign" colspan="2">ﻧﻌﻢ</td>
					<td class="noBorder sign" colspan="6">لا</td>
				@else
					<td class="noBorder sign" colspan="2">ﻧﻌﻢ</td>
					<td class="noBorder" colspan="2">ﻣﻦ ﻓﻀﻠﻚ اﺫﻛﺮ الاﺳﻢ :</td>
					<td class="noBorder" style="text-align: right" colspan="4">{{$applicant->hRelativesN}}</td>
				@endif
			</tr>
			<tr>
				<td class="noBorder" colspan="2">ﻛﻴﻒ ﻋﻠﻤﺖ ﻋﻦ ﺳﺎﺱ ﺇﻳﺠﻴﺒﺖ ؟</td>
				<td class="noBorder @if($applicant->howFind==1) sign @else unsign @endif" colspan="2">معارف</td>
				<td class="noBorder @if($applicant->howFind==2) sign @else unsign @endif" colspan="1">التجوال</td>
				<td class="noBorder @if($applicant->howFind==3) sign @else unsign @endif" colspan="2">اعلان / انترنت</td>
				<td class="noBorder @if($applicant->howFind==4) sign @else unsign @endif" colspan="1">اعلان / جريدة</td>
				<td class="noBorder @if($applicant->howFind==5) sign @else unsign @endif" colspan="2">ﻣﻮﻗﻊ اﻟﺸﺮﻛﺔ</td>
			</tr>
			<tr>
				<td class="noBorder" colspan="2">ﻣﺘﻰ ﺗﺴﺘﻄﻴﻊ ﺑﺪء اﻟﻌﻤﻞ ﻓﻰ اﻟﺸﺮﻛﺔ ؟</td>
				<td class="noBorder @if($applicant->startwork==0) sign @else unsign @endif" colspan="2">ﺃﺳﺒﻮﻉ</td>
				<td class="noBorder @if($applicant->startwork==1) sign @else unsign @endif" colspan="1">ﺃﺳﺒﻮﻋﻴﻦ</td>
				<td class="noBorder @if($applicant->startwork==2) sign @else unsign @endif" colspan="3">ﺷﻬﺮ</td>
				<td class="noBorder @if($applicant->startwork==3) sign @else unsign @endif" colspan="2">ًﻓﻮﺭا</td>
			</tr>
		</tr>
		<!-- \Other Section -->
		<tr>
			<td colspan="10" class="text-center upper-border">ﻓﻘﻂ ﻹﺳﺘﻌﻤﺎﻝ اﻹﺩاﺭﺓ</td>
		</tr>
		<tr>
			<td>التقييم المبدئى</td>
			<td class="text-center" colspan="3">{{$applicant->Frate}}</td>
			<td class="text-center" colspan="6"></td>
		</tr>
		<tr>
			<td>ﺣﺎﻟﺔ اﻟﻤﻘﺎﺑﻠﺔ :</td>
			<td class="@if($applicant->interSat==0) sign @else unsign @endif">ﻣﻮاﻓﻖ ﻋﻠﻴﻪ</td>
			<td class="@if($applicant->interSat==1) sign @else unsign @endif">ﻣﺮﻓﻮﺽ</td>
			<td class="@if($applicant->interSat==2) sign @else unsign @endif">اﻧﺘﻈﺎﺭ</td>
			<td>ﺑﻮاﺳﻄﺔ</td>
			<td>
				@if($applicant->interBy=='1') محمد زهرة @endif
				@if($applicant->interBy=='2') حسام عزت @endif
				@if($applicant->interBy=='3') هانى عصام @endif
				@if($applicant->interBy=='4') عمرو الجزار @endif
				@if($applicant->interBy=='5') هانى محمد @endif
				@if($applicant->interBy=='6') مؤمن عبد الرحم @endif
			</td>
			<td>تاريخ القبول</td>
			<td  colspan="2">
				{{$applicant->interDate}}
			</td>
		</tr>
		<tr>
			<td>ﺃﺳﻢ اﻟﻤﺎﺭﻛﺔ :</td>
			<td colspan="2">{{$applicant->JobBrand}}</td>
			<td colspan="2">اﻟﻔﺮﻉ :</td>
			<td>
				@if($applicant->JobBranch=='1') عباس العقاد @endif
				@if($applicant->JobBranch=='2') التجمع @endif
				@if($applicant->JobBranch=='3') المهندسين @endif
				@if($applicant->JobBranch=='4') مول العرب @endif
				@if($applicant->JobBranch=='5') مول مصر @endif
			</td>
			<td colspan="2">المسمى الوظيفي</td>
			<td colspan="2">{{$applicant->JobTitle}}</td>
		</tr>
		<tr style="text-align: center">
			<td style="border-bottom: 0" colspan="4">ﺗﻮﻗﻴﻊ ﻣﺪﻳﺮ اﻟﻤﻮاﺭﺩ اﻟﺒﺸﺮﻳﺔ</td>
			<td style="border-bottom: 0" colspan="6">ﺗﻮﻗﻴﻊ ﻣﺪﻳﺮ اﻟﻤﺎﺭﻛﺔ</td>
		</tr>
		<tr style="height: 35pt">
			<td colspan="4"></td>
			<td colspan="6"></td>
		</tr>
		<!-- \Auth Section -->
	</table>
</div>
</body>
</html>