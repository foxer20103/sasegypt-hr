@extends('layouts.app')
@section('title','Add Application')
@section('addApplication','class="active"')
@section('cssSection')
	<!-- bootstrap datepicker -->
	<link rel="stylesheet" href="{{asset('plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
	<!-- Select2 -->
	<link rel="stylesheet" href="{{asset('plugins/select2/select2.min.css')}}"/>
	<!-- DataTables -->
	<link rel="stylesheet" href="{{asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
	<!-- DataTables -->
	<link rel="stylesheet" href="{{asset('css/style.css')}}">
	<style>
		table#example {
			table-layout: fixed;
		}

		#example td {
			overflow: hidden;
			text-overflow: ellipsis;
		}

		#example .form-control {
			width: 100%;
		}
	</style>
@endsection
@section('jsSection')
	<!-- InputMask -->
	<script src="{{asset('plugins/input-mask/jquery.inputmask.js')}}"></script>
	<script src="{{asset('plugins/input-mask/jquery.inputmask.date.extensions.js')}}"></script>
	<!-- Select2 -->
	<script src="{{asset('plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>
	<script src="{{asset('plugins/select2/select2.min.js')}}"></script>
	<!-- page script -->
	<script src="{{asset('js/script.js')}}"></script>
	<!-- DataTables -->
	<script src="{{asset('plugins/datatables.net/js/jquery.dataTables.min.js')}}"></script>
	<script src="{{asset('https://cdn.datatables.net/rowreorder/1.2.3/js/dataTables.rowReorder.min.js')}}"></script>

	<script src="{{asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
	<script>
	 function CheckSid(element) {
		 var sid = $(element).val();
		 $.ajax({
			 type: "GET",
			 url: '{{route("check")}}',
			 data: {sid: sid},
			 dataType: "json",
			 success: function (res) {
				 if (res.sid) {
					 if (confirm('This Applicant Already Exist Do You Want continue?')) {
						 window.location = '/app/' + res.sid + '/edit';
					 } else {
						 return false;
					 }
				 } else {
					 alert('false');
				 }
			 },
			 error: function (jqXHR, exception) {

			 }
		 });
	 }
	$(document).ready(function() {
		/** Exp Table */
		var table = $('#example').DataTable({
			dom: '<"wrapper"t>',
			ordering:true,
			rowReorder: true,
			"iDisplayLength":50,
			columnDefs: [
				{ orderable: true, className: 'reorder'},
				{ orderable: false, targets: '_all' }
			],
			"initComplete": function(oSettings) {
			$(this).on('click', "button.row-remove", function(e) {
			  table.row($(this).closest('tr')).remove().draw();
			});
		  }
		});
		 var i = 0;
		 $('a#add_row').click(function () {
			 table.row.add([
				 '<span class="icon-drag"><span class="hide-id-drop">'+i+'</span></span>',
				 '<input name="company_name' + i + '" autocomplete="organiztion" type="text" value="{{old("company_name' + i +'")}}" placeholder="اﺳﻢ ﺟﻬﺔ اﻟﻌﻤﻞ" class="form-control"/>',
				 '<input name="jop_name' + i + '" type="text" value="{{old("jop_name' + i +'")}}" placeholder="اﻟﻮﻇﻴﻔﺔ" class="form-control"/>',
				 '<input name="time' + i + '" type="text" class="form-control" />',
				 '<input name="salary' + i + '" type="text" value="{{old("salary' + i +'")}}" placeholder="اﻟﻤﺮﺗﺐ الاﺳﺎﺳﻰ" class="form-control"/>',
				 '<input name="left_reson' + i + '" type="text" value="{{old("left_reson' + i +'")}}" placeholder="اﺳﺒﺎﺏ ﺗﺮﻙ اﻟﻌﻤﻞ" class="form-control"/>',
				 '<button class="btn btn-danger glyphicon glyphicon-remove row-remove"></button>'
			 ]).draw(false);
			 i++;
		 });
	table.on('row-reorder', function(e, details, edit){
	   for(var i = 0; i < details.length; i++){
	   	var td=$(details[i].node).find('td');
	   	for (var i = td.length - 1; i >= 0; i--) {
	   		console.log(td[i]);
	   	}

	   	console.log(edit);
	   }
	});

	 });
	</script>
@endsection

@section('content')
	<div class="col-sm-11">
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">اﻧﺸﺎء ﻃﻠﺐ ﺗﻮﻇﻴﻒ</h3>
			</div>
			@include('layouts.message')
			<form class="form-horizontal" method="post" action="{{route('app.store')}}">
			{{ csrf_field() }}
			<!-- /CSRFS -->
				<div class="box-body">
					<div class="box-header with-border">
						<p class="h4 box-title ">
							<u>اﻟﺒﻴﺎﻧﺎﺕ الاﺳﺎﺳﻴﺔ:</u>
						</p>
					</div>
					<div class="form-group">
						<!-- Name -->
						<label for="appName" class="col-sm-2 control-label">الاﺳﻢ</label>
						<div class="col-sm-4">
							<input type="text" value="{{old('name')}}" class="form-control" id="appName" autocomplete="name" name="name"
								   placeholder="اﺩﺧﻞ اﺳﻤﻚ ﺭﺑﺎﻋﻰ" required>
						</div>
						<!-- Birth Date -->
						<label for="dmask" class="col-sm-3 control-label">ﺗﺎﺭﻳﺦ الميلاد:</label>
						<div class="col-sm-3">
							<input type="text" value="{{old('b_date')}}" id="dmask" class="form-control" name="b_date"
								   placeholder="اﺩﺧﻞ ﺗﺎﺭﻳﺦ ﻣﻲلاﺩﻙ"
								   dir="ltr" required>
						</div>
					</div>
					<!-- /Name & B.Date -->
					<div class="form-group">
						<label for="mmask" class="col-sm-2 control-label">ﺭﻗﻢ اﻟﻤﻮﺑﺎﻳﻞ:</label>
						<div class="col-sm-4">
							<input type="text" value="{{old('mobile')}}" id="mmask" class="form-control" name="mobile"
								   placeholder="اﺩﺧﻞ ﺭﻗﻢ اﻟﻤﻮﺑﺎﻳﻞ"
								   dir="ltr" autocomplete="tel" required>
						</div>
						<label for="mmask" class="col-sm-3 control-label">ﺭﻗﻢ اﻟﻤﻮﺑﺎﻳﻞ:</label>
						<div class="col-sm-3">
							<input type="text" value="{{old('mobile2')}}" id="mmask2" class="form-control" name="mobile2"  autocomplete="tel" placeholder="اﺩﺧﻞ ﺭﻗﻢ اﻟﻤﻮﺑﺎﻳﻞ" dir="ltr">
						</div>
					</div>
					<!-- /Mobiles -->
					<div class="form-group">
						<label for="area" class="col-sm-2 control-label">اﻟﻤﻨﻄﻘﺔ اﻟﺴﻜﻨﻴﺔ:</label>
						<div class="col-sm-4">
							<select id="area" name="area" class="form-control select2" required>
								<option selected="selected" disabled="disabled">المنطقة السكنية</option>
								@foreach ($areas as $area)
									<option value="{{$area['area_id']}}">{{$area['name']}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<!-- /Area -->
					<div class="form-group">
						<label class="col-sm-2 control-label">اﻟﺠﻨﺲ:</label>
						<div class="col-sm-4">
							<div class="btn-group" role="group">
								<label class="btn btn-default form-label active">
									<input id="gMale" type="radio" value="1" checked="checked" name="gender" class="radio_btn" required>
									<i class="fa fa-male"></i>ﺫﻛﺮ
								</label>
								<label class="btn btn-default form-label">
									<input id="gfemale" type="radio" value="0" name="gender" class="radio_btn">
									<i class="fa fa-female"></i>ﺃﻧﺜﻰ
								</label>
							</div>
						</div>
						<!-- \Sex -->
						<label for="military" class="col-sm-3 control-label">اﻟﺤﺎﻟﺔ الاﺟﺘﻤﺎﻋﻴﺔ:</label>
						<div class="col-sm-3">
							<select name="marital" id="opMale" class="form-control" style="width: 100%;" required>
								<option value="0">اﻋﺰﺏ</option>
								<option value="1">ﻣﺘﺰﻭﺝ</option>
								<option value="2">ﻣﻄﻠﻖ</option>
								<option value="3">خاطب</option>
							</select>
							<select name="marital" id="opFemale" class="form-control" style="width: 100%;" required>
								<option value="0">اﻧﺴﺔ</option>
								<option value="1">ﻣﺘﺰﻭﺟﺔ</option>
								<option value="2">ﻣﻄﻠﻘﺔ</option>
								<option value="3">مخطوبة</option>
							</select>
						</div>
					</div>
					<!-- /Sex & Marital  -->
					<div class="form-group">
						<label for="military" id="military" class="col-sm-2 control-label">اﻟﺨﺪﻣﺔ اﻟﻌﺴﻜﺮﻳﺔ:</label>
						<div class="col-sm-5" id="military">
							<div class="radio-inline">
								<input type="radio" name="military" id="military" value="0" checked="checked" required> اﺩﻯ اﻟﺨﺪﻣﺔ
							</div>
							<div class="radio-inline">
								<input type="radio" name="military" id="optionsRadios2" value="1"> اﻋﻔﺎء ﻣﺆﻗﺖ
							</div>
							<div class="radio-inline">
								<input type="radio" name="military" id="optionsRadios2" value="2">اﻋﻔﺎء ﻧﻬﺎﺋﻰ
							</div>
							<div class="radio-inline">
								<input type="radio" name="military" id="optionsRadios2" value="3">ﻟﻢ ﻳﺼﺒﻪ اﻟﺪﻭﺭ
							</div>
						</div>
						<!-- /Miltry Service -->
						<label for="cDrive" class="col-sm-2 control-label">ﺭﺧﺼﺔ اﻟﻘﻴﺎﺩﺓ:</label>
						<div class="col-sm-3">
							<div class="btn-group" role="group">
								<label class="btn btn-default form-label">
									<input id="cDrive" type="radio" value="1" name="cdrive" class="radio_btn" required>ﻧﻌﻢ
								</label>
								<label class="btn btn-default form-label active">
									<input id="cDrive" type="radio" value="0" checked="checked" name="cdrive" class="radio_btn">لا
								</label>
							</div>
						</div>
					</div>
					<!-- /Drive Lenses Service -->
					<!-- /Miltry Service & Drive Lenses Service -->
					<div class="box-header with-border">
						<p class="h4 box-title">
							<u>اﻟﺪﺭﺟﺔ اﻟﻌﻠﻤﻴﺔ:</u>
						</p>
					</div>
					<div class="edu">
						<!-- Edu -->
						<div class="form-group">
							<div class="col-xs-10">
								<div class="btn-group" role="group">
									<label class="btn btn-default form-label active">
										<input id="cDrive" type="radio" value="1" checked="checked" name="edu" class="radio_btn" required>خريج
									</label>
									<label class="btn btn-default form-label">
										<input id="cDrive" type="radio" value="0" name="edu" class="radio_btn">طالب
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="university" class="col-sm-3 control-label">اﺳﻢ اﻟﺠﺎﻣﻌﺔ / اﻟﻤﻌﻬﺪ / اﻟﻤﺪﺭﺳﺔ</label>
							<div class="col-sm-6">
								<input type="text" value="{{old('university')}}" id="university" name="university" class="form-control" required
									   placeholder="اﺩﺧﻞ اﺳﻢ اﺧﺮ ﻣﻜﺎﻥ ﺩﺭﺳﺖ ﺑﻪ">
							</div>
						</div>
						<div class="form-group">
							<label for="college" class="col-sm-3 control-label">اﻟﺪﺭﺟﺔ اﻟﻌﻠﻤﻴﺔ</label>
							<div class="col-sm-6">
								<input type="text" value="{{old('college')}}" id="college" name="college" class="form-control" required
									   placeholder="اﺩﺧﻞ اﺳﻢ اﺧﺮ ﺩﺭﺟﺔ ﻋﻠﻤﻴﺔ ﺣﺼﻠﺖ ﻋﻠﻴﻬﺎ">
							</div>
						</div>
						<div class="form-group graduation_year">
							<label for="graduation_year" class="col-sm-3 control-label">ﺳﻨﺔ التخرج</label>
							<div class="col-sm-6">
								<input type="text" value="{{old('graduation_year','2017')}}" id="graduation_year" name="graduation_year"
									   class="form-control" required placeholder="اﺩﺧﻞ ﺳﻨﺔ اﻟﺤﺼﻮﻝ ﻋﻠﻴﻬﺎ">
							</div>
						</div>
						<div class="form-group college-year" style="display: none;">
							<label for="college-year" class="col-sm-3 control-label">الفرقة الدراسية</label>
							<div class="col-sm-6">
								<input type="number" min="1" max="5" value="{{old('college_year','1')}}" id="college-year" name="college_year"
									   class="form-control" placeholder="اﺩﺧﻞ الفرقة الدراسية">
							</div>
						</div>
					</div>
					<!-- /Education Section -->
					<div class="form-group">
						<label for="military" class="col-sm-3 control-label">ﻣﻬﺎﺭاﺕ اﻟﺤﺎﺳﺐ الاﻟﻰ:</label>
						<div class="col-sm-6">
							<select name="cskills" class="form-control" style="width: 100%;" required>
								<option value="0">ﻣﺒﺘﺪﻯء</option>
								<option value="1">ﻣﻘﺒﻮﻝ</option>
								<option value="2">جيد</option>
								<option value="3">جيد جدا</option>
								<option value="4">ﻣﻤﺘﺎﺯ</option>
							</select>
						</div>
					</div>
					<!-- /Computer Skills -->
					<div class="box-header with-border">
						<p class="h4 box-title">
							<u>اﻟﺨﺒﺮﺓ اﻟﻌﻤﻠﻴﺔ:</u>
						</p>
					</div>
					<div class="form-group">
						<a id="add_row" class="col-sm-2 btn btn-default pull-right">اﺿﺎﻓﺔ ﺧﺒﺮﺓ</a>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							<table id="example" class="table table-bordered table-hover">
								<thead>
								<tr>
									<th style="width:2%">#</th>
									<th class="text-center">ﺇﺳﻢ ﺟﻬﺔ اﻟﻌﻤﻞ</th>
									<th class="text-center">اﻟﻮﻇﻴﻔﺔ</th>
									<th class="text-center">فترة العمل</th>
									<th class="text-center">اﻟﻤﺮﺗﺐ اﻷﺳﺎﺳﻰ</th>
									<th class="text-center">ﺃﺳﺒﺎﺏ ﺗﺮﻙ اﻟﻌﻤﻞ</th>
									<th class="text-center remove_width">ﺣﺬﻑ ﺧﺒﺮﺓ</th>
								</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
					<!-- /Exp Table -->
					<div class="form-group">
						<label for="onjop" class="col-sm-4 control-label">ﻫﻞ اﻧﺖ ﻣﻮﻇﻒ ﺣﺎﻟﻴﺎً؟ </label>
						<div class="col-sm-5">
							<select name="onjop" id="onjop" class="form-control" style="width: 100%;" required>
								<option value="0">ﻧﻌﻢ</option>
								<option value="1" selected>لا</option>
							</select>
						</div>
					</div>
					<!-- /Computer Skills -->
					<div class="form-group">
						<label for="startwork" class="col-sm-4 control-label">ﻣﺘﻰ ﺗﺴﺘﻄﻴﻊ ﺑﺪء اﻟﻌﻤﻞ ﻓﻰ اﻟﺸﺮﻛﺔ ؟ </label>
						<div class="col-sm-5">
							<select name="startwork" id="startwork" class="form-control" style="width: 100%;" required>
								<option value="0">ﺃﺳﺒﻮﻉ</option>
								<option value="1">اﺳﺒﻮﻋﻴﻦ</option>
								<option value="2">ﺷﻬﺮ</option>
								<option value="3" selected>ﻓﻮﺭا</option>
							</select>
						</div>
					</div>
					<!-- /Computer Skills -->
					<div class="form-group">
						<label for="haveInsurance" class="col-sm-4 control-label">ﻫﻞ اﻧﺖ ﻣﺆﻣﻦ ﻋﻠﻴﻚ ﺗﺎﻣﻴﻦ اﺟﺘﻤﺎﻋﻲ ﺣﺎﻟﻴﺎ ؟ </label>
						<div class="col-sm-5">
							<select name="haveInsurance" id="haveInsurance" class="form-control" style="width: 100%;" required>
								<option value="1">ﻧﻌﻢ</option>
								<option value="2" selected>لا</option>
							</select>
						</div>
					</div>
					<!-- /Computer Skills -->
					<div class="form-group">
						<label for="havePension" class="col-sm-4 control-label">ﻫﻞ ﺗﺤﺼﻞ ﻋﻠﻰ ﻣﻌﺎﺵ ﺣﻜﻮﻣﻰ ؟ </label>
						<div class="col-sm-5">
							<select name="havePension" id="havePension" class="form-control" style="width: 100%;" required>
								<option value="1">ﻧﻌﻢ</option>
								<option value="2" selected>لا</option>
							</select>
						</div>
					</div>
					<!-- /Have Pension -->
					<div class="form-group">
						<label for="mmask" class="col-sm-4 control-label">ﻫﻞ ﻟﺪﻳﻚ ﺃﻗﺎﺭﺏ ﻳﻌﻤﻠﻮﻥ ﺑﺎﻟﺸﺮﻛﺔ؟</label>
						<div class="col-sm-3">
							<select name="hRelatives" id="hRelatives" class="form-control" required>
								<option value="1">ﻧﻌﻢ</option>
								<option value="2" selected>لا</option>
							</select>
						</div>
						<!-- Have Relatives -->
						<div class="hRelativesN">
							<label for="mmask" class="col-sm-2 control-label">من فضلك اذكر اﻻسم:</label>
							<div class="col-sm-3">
								<input type="text" value="{{old('hRelativesN')}}" class="form-control" name="hRelativesN" placeholder="من فضلك اذكر اﻻسم">
							</div>
						</div>
						<!-- Relatives Name -->
					</div>
					<!-- /Relatives -->
					<div class="form-group">
						<label for="howFind" class="col-sm-4 control-label">ﻛﻴﻒ ﻋﻠﻤﺖ ﻋﻦ ﺳﺎﺱ ﺇﻳﺠﻴﺒﺖ ؟ </label>
						<div class="col-sm-5">
							<select name="howFind" id="howFind" class="form-control" style="width: 100%;" required>
								<option value="1">ﺃﺻﺪﻗﺎء / ﺃﻗﺎﺭﺏ</option>
								<option value="2">اﺛﻨﺎء اﻟﺘﺠﻮاﻝ</option>
								<option value="3">اعلان / انترنت</option>
								<option value="4">اعلان / جريدة</option>
								<option value="5">ﻣﻮﻗﻊ اﻟﺸﺮﻛﺔ</option>
							</select>
						</div>
					</div>
					<!-- /How Find Us -->
					<div class="form-group">
						<label for="ExJop" class="col-sm-4 control-label">الوظيفة المحتملة</label>
						<div class="col-sm-3">
							<select name="ExJop" id="ExJop" class="form-control" style="width: 100%;" required>
								<option>Sales</option>
								<option>Stock</option>
								<option>Security Fitting</option>
								<option>Casher</option>
								<option>VM</option>
								<option>Assist</option>
								<option>Manger</option>
							</select>
						</div>
						<label for="Frate" class="col-sm-3 control-label">التقييم المبدئى</label>
						<div class="col-sm-3">
							<select name="Frate" id="Frate" class="form-control" required>
								<option>1</option>
								<option>2</option>
								<option>3</option>
								<option>4</option>
								<option>5</option>
								<option>6</option>
								<option>7</option>
								<option>8</option>
								<option>9</option>
								<option>10</option>
							</select>
						</div>
					</div>
					<!-- /Expected Jop -->
					<div class="form-group">
						<label for="notes" class="col-sm-4 control-label">ملاحظات</label>
						<div class="col-sm-5">
							<textarea name="notes" id="notes" cols="50" rows="5">{{old('college_year')}}</textarea>
						</div>
					</div>
					<!-- /Expected Jop -->
					<div class="form-group">
						<label for="apply" class="col-sm-4 control-label">التقديم</label>
						<div class="col-sm-3">
							<select name="apply" id="apply" class="form-control" required>
								<option>الادارة</option>
								<option>الفروع</option>
							</select>
						</div>
					</div>
					<!-- /Education Section -->
					<div class="form-group">
						<label for="appName" autocomplete="name" class="col-sm-2 control-label">السيرة الزاتية</label>
						<div class="col-sm-4">
							<label class="btn btn-default btn-file">
								Browse <input type="file" name="CV" style="display: none;">
							</label>
						</div>
					</div>

				</div>
				<!-- /.box-footer -->
				<div class="box-footer pull-left">
					<button type="reset" class="btn btn-default">اﻟﻐﺎء</button>
					<button type="submit" class="btn btn-info ">ﺣﻔﻆ</button>
				</div>
			</form>
		</div>
	</div>
	<!-- /.box -->
@endsection
