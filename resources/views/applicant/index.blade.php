@extends('layouts.app')
@section('title','Add Application')
@section('addApplication','class="active"')
@section('cssSection')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<meta name="csrf-token" content="{{csrf_token()}}">
@endsection

@section('jsSection')
<!-- DataTables -->
<script src="{{asset('plugins/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">
$('#dataTableBuilder').on('click', '.btn-delete[data-remote]', function (e) {
    e.preventDefault();
     $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var url = $(this).data('remote');
    // confirm then
    if (confirm('هل تريد حذف طلب التوظيف؟')) {
        $.ajax({
            url: url,
            type: 'DELETE',
            dataType: 'json',
            data: {method: '_DELETE', submit: true}
        }).always(function (data) {
            $('#dataTableBuilder').DataTable().draw(false);
            alert('تم حذف الطلب');
        });
    }else
        alert("تم الغاء الطلب");
});
</script>
<!-- page script -->
{!! $dataTable->scripts() !!}
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Main content -->
	<section class="content">
		<!-- Default box -->
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">عرض البيانات المسجلة</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="col-sm-10">
					<form id="search" class="form-horizontal" method="get" action="{{route('search')}}">
						<style type="text/css">.row-margin [class*="col-sm-"]{margin-bottom: 15px;}</style>
						<div class="form-group row row-margin">
							<div class="col-sm-3">
								<select name="area" class="form-control">
										<option disabled="disabled" selected>المنطقة</option>
										@foreach ($areas as $area)
											<option value="{{$area['area_id']}}" @if(request('area')==$area['area_id']) selected @endif
											>{{$area['name']}}</option>
										@endforeach
									</select>
							</div>
							<div class="col-sm-3">
								<select name="Frate" class="form-control">
										<option disabled="disabled" selected>التقييم</option>
										@for($i=1;$i<=10;$i++)
											<option @if(request('Frate')==$i) selected @endif>{{$i}}</option>
										@endfor
									</select>
							</div>
							<div class="col-sm-3">
								<select name="edu" class="form-control">
										<option disabled="disabled" selected>الدراسة</option>
										<option value="1" @if(request('edu')=="1") selected @endif>خريج</option>
										<option value="0" @if(request('edu')=="0") selected @endif>طالب</option>
									</select>
							</div>
							<div class="col-sm-3">
								<select name="gender" class="form-control">
										<option disabled="disabled" selected>الجنس</option>
										<option value="1" @if(request('gender')=="1") selected @endif>ذكر</option>
										<option value="0" @if(request('gender')=="0") selected @endif>انثي</option>
									</select>
							</div>
							<div class="col-sm-3">
								<select name="b_date" class="form-control">
										<option disabled="disabled" selected>السن</option>
										<option value="1" @if(request('b_date')=="1") selected @endif>اقل من 18 سنة</option>
										<option value="2" @if(request('b_date')=="2") selected @endif>من 18 الى 25</option>
										<option value="3" @if(request('b_date')=="3") selected @endif>اكثر من 25</option>
									</select>
							</div>
							<div class="col-sm-3">
								<select name="branch" class="form-control">
									<option disabled="disabled" selected value="">الفروع</option>
									@foreach ($branchs as $branch)
										<option value="{{$branch['id']}}" @if(request('branch')==$branch['id']) selected @endif
										>{{$branch['name']}}</option>
									@endforeach
								</select>
							</div>
							<div class="col-sm-3">
								<select name="created_at" class="form-control">
									<option disabled="disabled" selected value="">سنة التقديم</option>
									@for($i=2018;$i<=date("Y");$i++)
									<option @if(request('created_at')==$i) selected @endif value="{{$i}}">{{$i}}</option>
									@endfor
								</select>
							</div>
							<div class="col-sm-3">
								<select name="interSat" class="form-control">
									<option disabled="disabled" selected value="">حالة المقابلة</option>
									<option @if(request('interSat')=='0') selected @endif value="0">ﻣﻮاﻓﻖ ﻋﻠﻴﻪ</option>
									<option @if(request('interSat')=='1') selected @endif value="1">ﻣﺮﻓﻮﺽ</option>
									<option @if(request('interSat')=='2') selected @endif value="2">اﻧﺘﻈﺎﺭ</option>
								</select>
							</div>
							<div class="col-sm-3">
								<select name="apply" class="form-control">
									<option disabled="disabled" selected value="">مكان التقديم</option>
									<option @if(request('apply')=='الادارة') selected @endif value="الادارة">الادارة</option>
									<option @if(request('apply')=='الفروع') selected @endif value="الفروع">الفروع</option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</form>
				</div>
				<br/>
				{!! $dataTable->table() !!}
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
