<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="backup2/style.css">
</head>
<body class="A4">
  <section class="sheet">
    <div class="page">
      <!-- Header -->
      <div class="header">
        <span>ﻧﻤﻮﺫﺝ ﻃﻠﺐ ﻭﻇﻴﻔﺔ</span>
        @if(!empty($ProfileImage) and $ProfileImage !== null)
        <img src="{{$ProfileImage}}" />
        @endif
      </div>
      <!-- personal Info -->
      <div class="personalInfo">
        <h4>اﻟﺒﻴﺎﻧﺎﺕ اﻟﺸﺨﺼﻴﺔ</h4>
        <div class="info">
          <span class="head">ﺃﺳﻢ ﻣﻘﺪﻡ اﻟﻄﻠﺐ</span>
          <span>{{$name}}</span>
        </div>
        <div class="info">
          <span class="head">ﺗﺎﺭﻳﺦ اﻟﻤﻴﻼﺩ</span>
          <span>{{$BirthOfDate}}</span>
          <span style="padding-right: 20mm;" class="head">ﺗﺎﺭﻳﺦ اﻟﺘﻘﺪﻳﻢ</span>
          <span>{{$CreatedAt}}</span>
        </div>
        <div class="info">
          <span class="head">اﻟﻌﻨﻮاﻥ اﻟﺤﺎﻟﻰ</span>
          <span>{{$LivingArea}}</span>
        </div>
        <div class="info">
          <span class="head">ﺭﻗﻢ اﻟﻤﻮﺑﺎﻳﻞ</span>
          <span>{{$MobileNumber}}</span>
        </div>
        <div class="info">
          <span class="head">اﻟﺠﻨﺲ</span>
          <span>{{$Gender}}</span>
        </div>
        <div class="info">
          <span class="head">اﻟﺤﺎﻟﺔ اﻻﺟﺘﻤﺎﻋﻴﺔ</span>
          <span>{{$MaritalStatue}}</span>
        </div>
        @if($app->gender==1)
        <div class="info">
          <span class="head">اﻟﺨﺪﻣﺔ اﻟﻌﺴﻜﺮﻳﺔ</span>
          <span>{{$MilitaryStatue}}</span>
        </div>
        @endif
        <div class="info">
          <span class="head">ﺭﺧﺼﻪ اﻟﻘﻴﺎﺩﺓ</span>
          <span>{{$CanDrive}}</span>
        </div>
      </div>
      <!-- education Info -->
      <div class="educationInfo">
        <h4>اﻟﺪﺭﺟﺔ اﻟﻌﻤﻠﻴﺔ</h4>
        <div class="info">
          <span class="head">اﻟﺠﺎﻣﻌﺔ / اﻟﻤﻌﻬﺪ / اﻟﻤﺪﺭﺳﺔ</span>
          <span>{{$UniversityName}}</span>
        </div>
        <div class="info">
          <span class="head">اﻟﻜﻠﻴﺔ / اﻟﺪﺭﺟﺔ اﻟﻌﻠﻤﻴﺔ</span>
          <span>{{$CollegeName}}</span>
        </div>
        <div class="info">
          <span class="head">{{$StudyName}} </span>
          <span>{{$StudyYear}}</span>
        </div>
        <div class="info">
          <span class="head">ﻣﻬﺎﺭاﺕ اﻟﺤﺎﺳﺐ اﻷﻟﻰ</span>
          <span>{{$ComputerSkills}}</span>
        </div>
      </div>
      <!-- exp Info -->
      <div class="expInfo">
        <h4 style="text-align: center;">اﻟﺨﺒﺮاﺕ اﻟﺴﺎﺑﻘﺔ (ﻣﺒﺘﺪﺋﺎً ﺑﺄﺧﺮ ﻭﻇﻴﻔﺔ )</h4>
        <div class="exp" style="font-weight: 700;">
          <span class="td">ﺃﺳﻢ اﻟﺠﻬﺔ</span>
          <span class="td">اﻟﻮﻇﻴﻔﺔ</span>
          <span class="td">ﻓﺘﺮﺓ اﻟﻌﻤﻞ</span>
          <span class="td">اﻟﺮاﺗﺐ</span>
          <span class="td">ﺳﺒﺐ ﺗﺮﻙ اﻟﻮﻇﻴﻔﺔ</span>
        </div>
        @if(!empty($app->experiences['0']))
        @foreach($app->experiences as $exp)
        <div class="exp">
          <span class="td">{{$exp->company_name}}</span>
          <span class="td">{{$exp->jop_name}}</span>
          <span class="td">{{$exp->time}}</span>
          <span class="td">{{$exp->salary}}</span>
          <span class="td">{{$exp->left_reson}}</span>
        </div>
        @endforeach
        @else
        <div class="exp">
          <span class="td">ﻵ ﺗﻮﺟﺪ ﺧﺒﺮﺓ ﻟﻬﺬا اﻟﺸﺨﺺ</span>
        </div>
        @endif
      </div>
      <!-- exp Info -->
      <div class="Authonly">
        <h4 style="text-align: center;">ﻓﻘﻂ ﻹﺳﺘﻌﻤﺎﻝ اﻹﺩاﺭﺓ</h4>
        <div class="info">
          <span class="head">اﻟﺘﻘﻴﻴﻢ اﻟﻤﺒﺪﺋﻰ</span>
          <span>{{$Fristrate}}</span>
          <span></span>
          <span class="head">اﻟﻮﻇﻴﻔﺔ اﻟﻤﻘﺘﺮﺣﺔ</span>
          <span>{{$ExpectedJop}}</span>
        </div>
        <div class="info">
          <span class="head">ﺣﺎﻟﺔ اﻟﻤﻘﺎﺑﻠﺔ</span>
          <span>{{$interSat}}</span>
          <span class="head">ﺑﻮاﺳﻄﺔ</span>
          <span>{{$interBy}}</span>
          <span class="head">ﺗﺎﺭﻳﺦ اﻟﻘﺒﻮﻝ</span>
          <span>{{$interDate}}</span>
        </div>
        <div class="info">
          <span class="head">ﺃﺳﻢ اﻟﻤﺎﺭﻛﺔ</span>
          <span>{{$JobBranch}}</span>
          <span class="head">اﻟﻔﺮﻉ</span>
          <span>{{$JobBrand}}</span>
          <span class="head">اﻟﻤﺴﻤﻰ اﻟﻮﻇﻴﻔﻲ</span>
          <span>{{$JobTitle}}</span>
        </div>
      <div class="sign">
        <span>ﺗﻮﻗﻴﻊ ﻣﺪﻳﺮ اﻟﻤﻮاﺭﺩ اﻟﺒﺸﺮﻳﺔ</span>
        <span>ﺗﻮﻗﻴﻊ ﻣﺪﻳﺮ اﻟﻤﺎﺭﻛﺔ</span>
      </div>
      </div>
    </div>
  </section>
</body>
</html>
