    <table style="width: 590pt;height: 836pt;color: black;font-size: 13pt;font-weight: 700;font-style: normal;font-family: Arial, sans-serif;margin: 5pt 5pt;margin-bottom: 0px;border-collapse: collapse;">
      <tbody>
        <!-- Header -->
        <tr>
          <td class="no-border" style="height:150pt;text-align: center; font-size:28px;" colspan="5">ﻧﻤﻮﺫﺝ ﻃﻠﺐ ﻭﻇﻴﻔﺔ</td>
          <td style="text-align:center;border-right: 1px solid #000;width:150pt;" colspan="3">
          @if(!empty($ProfileImage) and $ProfileImage !== null)
            <img src="{{$ProfileImage}}" />
          @endif
          </td>
        </tr>
        <!-- Information Heading -->
        <tr>
          <td style="border-top:0;" class="h1" colspan="8">اﻟﺒﻴﺎﻧﺎﺕ اﻟﺸﺨﺼﻴﺔ</td>
        </tr>
        <tr>
          <td class="no-border" colspan="2">ﺃﺳﻢ ﻣﻘﺪﻡ اﻟﻄﻠﺐ</td>
          <td class="no-border border-left" colspan="6">{{$name}}</td>
        </tr>
        <tr>
          <td class="no-border" colspan="2">ﺗﺎﺭﻳﺦ اﻟﻤﻴﻼﺩ</td>
          <td class="no-border" colspan="2">{{$BirthOfDate}}</td>
          <td class="no-border" colspan="2">ﺗﺎﺭﻳﺦ اﻟﺘﻘﺪﻳﻢ</td>
          <td class="no-border border-left" colspan="2">{{$CreatedAt}}</td>
        </tr>
        <tr>
          <td class="no-border" colspan="2">اﻟﻌﻨﻮاﻥ اﻟﺤﺎﻟﻰ</td>
          <td class="no-border border-left" colspan="6">{{$LivingArea}}</td>
        </tr>
        <tr>
          <td class="no-border" colspan="2">ﺭﻗﻢ اﻟﻤﻮﺑﺎﻳﻞ</td>
          <td class="no-border border-left" colspan="6">{{$MobileNumber}}</td>
        </tr>
        <tr>
          <td class="no-border" colspan="2">اﻟﺠﻨﺲ</td>
          <td class="no-border border-left" colspan="6">{{$Gender}}</td>
        </tr>
        <tr>
          <td class="no-border" colspan="2">اﻟﺤﺎﻟﺔ اﻻﺟﺘﻤﺎﻋﻴﺔ</td>
          <td class="no-border border-left" colspan="6">{{$MaritalStatue}}</td>
        </tr>
        @if($app->gender==1)
        <tr>
          <td class="no-border" colspan="2">اﻟﺨﺪﻣﺔ اﻟﻌﺴﻜﺮﻳﺔ</td>
          <td class="no-border border-left" colspan="6">{{$MilitaryStatue}}</td>
        </tr>
        @endif
        <tr>
          <td class="no-border" colspan="2">ﺭﺧﺼﻪ اﻟﻘﻴﺎﺩﺓ</td>
          <td class="no-border border-left" colspan="6">{{$CanDrive}}</td>
        </tr>
        <!-- Education -->
        <tr>
          <td style="" class="h1" colspan="8">اﻟﺪﺭﺟﺔ اﻟﻌﻤﻠﻴﺔ </td>
        </tr>
        <tr>
          <td style="" colspan="3">اﻟﺠﺎﻣﻌﺔ / اﻟﻤﻌﻬﺪ / اﻟﻤﺪﺭﺳﺔ</td>
          <td class="border-left" colspan="5">{{$UniversityName}}</td>
        </tr>
        <tr>
          <td style="" colspan="3">اﻟﻜﻠﻴﺔ / اﻟﺪﺭﺟﺔ اﻟﻌﻠﻤﻴﺔ</td>
          <td style="" colspan="5">{{$CollegeName}}</td>
        </tr>
        <tr>
          <td style="" colspan="3">{{$StudyName}} </td>
          <td style="" colspan="5">{{$StudyYear}}</td>
        </tr>
        <tr>
          <td style="" colspan="3">ﻣﻬﺎﺭاﺕ اﻟﺤﺎﺳﺐ اﻷﻟﻰ</td>
          <td style="" colspan="5">{{$ComputerSkills}}</td>
        </tr>
        <!-- Exp -->
        <tr>
          <td style="text-align:center;" class="h1" colspan="8">اﻟﺨﺒﺮاﺕ اﻟﺴﺎﺑﻘﺔ (ﻣﺒﺘﺪﺋﺎً ﺑﺄﺧﺮ ﻭﻇﻴﻔﺔ )</td>
        </tr>
        <tr>
          <th style="padding: 5px;" colspan="2">ﺃﺳﻢ اﻟﺠﻬﺔ</th>
          <th style="padding: 5px;">اﻟﻮﻇﻴﻔﺔ</th>
          <th style="padding: 5px;" colspan="2">ﻓﺘﺮﺓ اﻟﻌﻤﻞ</th>
          <th style="padding: 5px;">اﻟﺮاﺗﺐ</th>
          <th style="padding: 5px;" colspan="2">ﺳﺒﺐ ﺗﺮﻙ اﻟﻮﻇﻴﻔﺔ</th>
        </tr>
        @if(!empty($app->experiences['0']))
        @foreach($app->experiences as $exp)
        <tr style="text-align:center;">
          <td style="overflow-wrap: break-word;padding: 5px;width:156px;" colspan="2">{{$exp->company_name}}</td>
          <td style="overflow-wrap: break-word;padding: 5px;width:156px;">{{$exp->jop_name}}</td>
          <td style="overflow-wrap: break-word;padding: 5px;width:156px;" colspan="2">{{$exp->time}}</td>
          <td style="overflow-wrap: break-word;padding: 5px;width:156px;">{{$exp->salary}}</td>
          <td style="overflow-wrap: break-word;padding: 5px;width:156px;" colspan="2">{{$exp->left_reson}}</td>
        </tr>
        @endforeach
        @else
        <tr style="text-align:center">
          <td colspan="8">ﻵ ﺗﻮﺟﺪ ﺧﺒﺮﺓ ﻟﻬﺬا اﻟﺸﺨﺺ</td>
        </tr>
        @endif
        <!-- Other -->
        <tr>
          <td style="text-align:center;" colspan="8">ﻓﻘﻂ ﻹﺳﺘﻌﻤﺎﻝ اﻹﺩاﺭﺓ</td>
        </tr>
        <tr>
          <td style="">اﻟﺘﻘﻴﻴﻢ اﻟﻤﺒﺪﺋﻰ</td>
          <td style="" colspan="3">{{$Fristrate}}</td>
          <td style="" colspan="2">اﻟﻮﻇﻴﻔﺔ اﻟﻤﻘﺘﺮﺣﺔ</td>
          <td style="" colspan="2">{{$ExpectedJop}}</td>
        </tr>
        <tr>
          <td style="">ﺣﺎﻟﺔ اﻟﻤﻘﺎﺑﻠﺔ</td>
          <td style="" colspan="2">{{$interSat}}</td>
          <td style="">ﺑﻮاﺳﻄﺔ</td>
          <td style="">{{$interBy}}</td>
          <td style="" colspan="2">ﺗﺎﺭﻳﺦ اﻟﻘﺒﻮﻝ</td>
          <td style="">{{$interDate}}</td>
        </tr>
        <tr>
          <td style="">ﺃﺳﻢ اﻟﻤﺎﺭﻛﺔ</td>
          <td style="" colspan="2">{{$JobBranch}}</td>
          <td style="">اﻟﻔﺮﻉ</td>
          <td style="">{{$JobBrand}}</td>
          <td style="" colspan="2">اﻟﻤﺴﻤﻰ اﻟﻮﻇﻴﻔﻲ</td>
          <td style="">{{$JobTitle}}</td>
        </tr>
        <tr>
          <td style="height:100pt;vertical-align:top;text-align:center;" colspan="5">ﺗﻮﻗﻴﻊ ﻣﺪﻳﺮ اﻟﻤﻮاﺭﺩ اﻟﺒﺸﺮﻳﺔ</td>
          <td style="height:100pt;vertical-align:top;text-align:center;" colspan="3">ﺗﻮﻗﻴﻊ ﻣﺪﻳﺮ اﻟﻤﺎﺭﻛﺔ</td>
        </tr>
    </table>
