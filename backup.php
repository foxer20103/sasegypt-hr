<?php

require '../vendor/autoload.php';

use Ifsnop\Mysqldump as IMysqldump;

try {
    $settings = ['add-drop-table' => true, 'lock-tables' => false];
    $dump     = new IMysqldump\Mysqldump('mysql:host=127.0.0.1;dbname=sas', 'root', 'test', $settings);
    $dump->start('dumps.sql');
} catch (\Exception $e) {
    echo 'mysqldump-php error: ' . $e->getMessage();
}
