<?php

use App\Applicants\applicant;
use Faker\Generator as Faker;

$factory->define(applicant::class, function (Faker $faker) {
    return [
        'name'            => $faker->name,
        'b_date'          => $faker->date,
        'mobile'          => '011' . $faker->ean8,
        'mobile2'         => '011' . $faker->ean8,
        'area'            => random_int(\DB::table('areas')->min('area_id'), \DB::table('areas')->max('area_id')),
        'gender'          => $faker->boolean,
        'military'        => $faker->numberBetween(0, 3),
        'marital'         => $faker->numberBetween(0, 3),
        'cdrive'          => $faker->boolean,
        'edu'             => $faker->boolean,
        'university'      => $faker->sentence(3),
        'college'         => $faker->sentence(2),
        'cskills'         => $faker->numberBetween(0, 4),
        'onjop'           => $faker->boolean,
        'startwork'       => $faker->boolean,
        'havePension'     => $faker->numberBetween(1, 2),
        'howFind'         => $faker->numberBetween(1, 5),
        'haveInsurance'   => $faker->numberBetween(1, 2),
        'graduation_year' => $faker->year,
        'college_year'    => $faker->numberBetween(1, 5),
        'hRelatives'      => $faker->numberBetween(1, 2),
        'hRelativesN'     => $faker->name,
        'ExJop'           => $faker->randomElement(['Sales', 'Stock', 'Security Fitting', 'Casher', 'VM', 'Assist', 'Manger']),
        'Frate'           => $faker->numberBetween(1, 10),
        'notes'           => $faker->sentence(5),
        'sid'             => $faker->unique()->numberBetween(10000000000000, 99999999999999),
        'interSat'        => $faker->numberBetween(0, 2),
        'interBy'         => $faker->numberBetween(1, 6),
        'interDate'       => $faker->date,
        'JobBrand'        => $faker->randomElement(['Terranova', 'Calliope']),
        'JobTitle'        => $faker->randomElement(['Sales', 'Stock', 'Security Fitting', 'Casher', 'VM', 'Assist', 'Manger']),
        'JobBranch'       => $faker->numberBetween(1, 5),
        'apply'           => $faker->randomElement(['الادارة', 'الفروع']),
    ];
});
