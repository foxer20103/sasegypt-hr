<?php

use Faker\Generator as Faker;

$factory->define(App\Applicants\experience::class, function (Faker $faker) {
    return [
        'user_id'      => random_int(\DB::table('applicants')->min('id'), \DB::table('applicants')->max('id')),
        'company_name' => $faker->sentence(2),
        'jop_name'     => $faker->word,
        'expCat'       => $faker->numberBetween(1, 11),
        'time'         => $faker->sentence(5),
        'salary'       => $faker->numberBetween(1000, 50000),
        'left_reson'   => $faker->sentence(3),
    ];
});
