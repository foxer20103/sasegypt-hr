<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_write       = new Role();
        $role_write->name = 'write';
        $role_write->save();
        $role_delete       = new Role();
        $role_delete->name = 'delete';
        $role_delete->save();
        $role_backup       = new Role();
        $role_backup->name = 'backup';
        $role_backup->save();
        $role_admin       = new Role();
        $role_admin->name = 'admin';
        $role_admin->save();
    }
}
