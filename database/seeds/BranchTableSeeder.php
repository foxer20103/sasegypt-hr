<?php
use App\Applicants\branch;
use Illuminate\Database\Seeder;

class BranchTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        branch::insert([
            ['name' => 'ﻋﺒﺎﺱاﻟﻌﻘﺎﺩ'],
            ['name' => 'اﻟﺘﺠﻤﻊ'],
            ['name' => 'المعادى'],
            ['name' => 'اﻟﻤﻬﻨﺪﺳﻴﻦ'],
            ['name' => 'ﻣﻮﻝاﻟﻌﺮﺏ'],
            ['name' => 'ﻣﻮﻝﻣﺼﺮ'],
        ]);
    }
}
