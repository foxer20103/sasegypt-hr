<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicants', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('area')->nullable();
            $table->string('mobile');
            $table->string('mobile2')->nullable();
            $table->date('b_date');
            $table->boolean('gender');
            $table->integer('marital');
            $table->integer('military');
            $table->boolean('cdrive');
            $table->boolean('edu');
            $table->string('university');
            $table->string('college');
            $table->integer('college_year')->nullable();
            $table->integer('graduation_year')->nullable();
            $table->integer('cskills');
            $table->boolean('onjop');
            $table->integer('startwork');
            $table->integer('haveInsurance');
            $table->integer('havePension');
            $table->integer('hRelatives');
            $table->string('hRelativesN')->nullable();
            $table->integer('howFind');
            $table->string('ExJop');
            $table->integer('Frate');
            $table->text('notes')->nullable();
            $table->string('img')->nullable();
            $table->string('cv')->nullable();
            $table->integer('interSat')->nullable();
            $table->integer('interBy')->nullable();
            $table->date('interDate')->nullable();
            $table->string('JobBrand')->nullable();
            $table->integer('JobBranch')->nullable();
            $table->string('JobTitle')->nullable();
            $table->string('apply');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applicants');
    }
}
